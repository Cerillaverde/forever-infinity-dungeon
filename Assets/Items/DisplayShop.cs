using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class DisplayShop : MonoBehaviour
{
    [SerializeField] private GameObject displayBackpackParent;

    [SerializeField] private GameObject displayItemPrefab;

    [SerializeField] private Backpack[] mochilas;
    //[SerializeField] private Backpack backpack;
    [SerializeField] private ShopController shopController;

    [SerializeField] private TextMeshProUGUI namePlayer;
    

    private void ClearDisplay()
    {
        foreach (Transform child in displayBackpackParent.transform)
            Destroy(child.gameObject);
    }

    private void FillDisplay()
    {
        if (shopController.GetNumPlayer() == 1)
        {
            namePlayer.text = "Player 1";
            foreach (Backpack.ItemSlot itemSlot in mochilas[0].ItemSlots)
            {
                GameObject displayedItem = Instantiate(displayItemPrefab, displayBackpackParent.transform);
                
                displayedItem.GetComponent<ItemShop>().Load(itemSlot);
            }
        }

        if (shopController.GetNumPlayer() == 2)
        {
                namePlayer.text = "Player 2";
                foreach (Backpack.ItemSlot itemSlot in mochilas[1].ItemSlots)
                {
                    GameObject displayedItem = Instantiate(displayItemPrefab, displayBackpackParent.transform);
                    
                    displayedItem.GetComponent<ItemShop>().Load(itemSlot);
                }
        }
        
    }

    public void RefreshShop()
    {
        ClearDisplay();
        FillDisplay();
    }
}