using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IConsumible : IStackable
{
    public void UsedBy(GameObject go);
}