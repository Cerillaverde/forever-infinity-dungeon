using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Healing Item", menuName = "Inventory/Items/Healing")]
public class HealingObject : ConsumibleClass
{
    [Space] [Header("Healing Capacity")] [Space] [SerializeField]
    private int heal;

    public override void UsedBy(GameObject nPlayer)
    {
        nPlayer.GetComponent<PlayerController>().HealHp(heal);
    }
}