using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEquipable
{
    public bool Equip(GameObject go);
    public bool Unequip(GameObject go);

    public void isEquiped(bool a);

    public bool getEquiped();
}