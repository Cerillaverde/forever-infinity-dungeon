using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ConsumibleClass : ObjectClass, IConsumible
{
    [SerializeField] [Space] private int maxStack;

    public int GetMaxStack()
    {
        return maxStack;
    }

    public abstract void UsedBy(GameObject nPlayer);
}