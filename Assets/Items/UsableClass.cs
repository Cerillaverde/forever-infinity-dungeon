using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UsableClass : ObjectClass, IUsable
{
    public bool Use()
    {
        throw new System.NotImplementedException();
    }
}