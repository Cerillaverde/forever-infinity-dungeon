using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon Item", menuName = "Inventory/Items/Weapons")]
public class WeaponObject : EquipableClass
{
    [Space] [Header("Attack Capacity")] [Space] 
    [SerializeField] private int attackDmg;

    public override bool Equip(GameObject go)
    {
        // if (go.GetComponent<PlayerController>().Weapon != null)
        // {
        //     Debug.Log("You already have a weapon equiped.");
        //     return false;
        // }

        go.GetComponent<PlayerController>().SetAttack((go.GetComponent<PlayerController>().GetAttack() - go.GetComponent<PlayerController>().Weapon.attackDmg) + attackDmg);
        go.GetComponent<PlayerController>().Weapon.equiped = false;
        go.GetComponent<PlayerController>().Weapon = this;
        equiped = true;
        return true;
    }

    public override bool Unequip(GameObject go)
    {
        // go.GetComponent<PlayerController>().SetAttack(go.GetComponent<PlayerController>().GetAttack() - attackDmg);
        Debug.Log("Weapons dequiped");
        equiped = false;
        return true;
    }
}