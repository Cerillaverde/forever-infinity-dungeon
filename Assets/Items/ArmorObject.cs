using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Armor Item", menuName = "Inventory/Items/Armors")]
public class ArmorObject : EquipableClass
{
    [Space] [Header("Defensive Capacity")] [Space] [SerializeField]
    private int defenseStat;

    public override bool Equip(GameObject go)
    {
        // if (go.GetComponent<PlayerController>().Armor != null)
        //     return false;

        go.GetComponent<PlayerController>().SetDefense(go.GetComponent<PlayerController>().GetDefense() - go.GetComponent<PlayerController>().Armor.defenseStat + defenseStat);
        go.GetComponent<PlayerController>().Armor.equiped = false;
        go.GetComponent<PlayerController>().Armor = this;
        equiped = true;
        return true;
    }

    public override bool Unequip(GameObject go)
    {
        // go.GetComponent<PlayerController>().SetDefense(go.GetComponent<PlayerController>().GetDefense() - defenseStat);
        // go.GetComponent<PlayerController>().Armor = null;
        equiped = false;
        return true;
    }
}