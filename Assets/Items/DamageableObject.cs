using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Damaging Item", menuName = "Inventory/Items/Damaging")]
public class DamageableObject : ConsumibleClass
{
    [Space] [Header("Damaging Capacity")] [Space] [SerializeField]
    private int damage;

    public override void UsedBy(GameObject nPlayer)
    {
       int random= Random.Range(1, 3);
       if (random == 1)
       {
           nPlayer.GetComponent<PlayerController>().LooseHp(damage);  
       }else if (random == 2)
       {
           nPlayer.GetComponent<PlayerController>().HealHp(damage);  
       }
        
        
    }
}