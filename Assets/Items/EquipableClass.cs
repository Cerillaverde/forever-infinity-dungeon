using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EquipableClass : ObjectClass, IEquipable
{
    public bool equiped;
    public abstract bool Equip(GameObject go);
    public abstract bool Unequip(GameObject go);

    public void isEquiped(bool b)
    {
        equiped = b;
    }

    public bool getEquiped()
    {
        return equiped;
    }
}