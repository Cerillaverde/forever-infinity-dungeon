using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjectClass : ScriptableObject
{
    [Header("Item Properties")] [Space] [Space] [SerializeField]
    private int id;

    [Space] [SerializeField] private string itemName;
    [Space] [SerializeField] private Sprite icon;

    [Space] [SerializeField] private Sprite sprite;

    [Space] [SerializeField] public int price;

    public bool fastAcces = false;

    public int getId()
    {
        return id;
    }

    public string getName()
    {
        return itemName;
    }

    public Sprite getIcon()
    {
        return icon;
    }

    public Sprite getSprite()
    {
        return sprite;
    }
}