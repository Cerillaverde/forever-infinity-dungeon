public interface IHealable
{
    public bool HealHp(int healing);
    public bool LooseHp(int healing);
}