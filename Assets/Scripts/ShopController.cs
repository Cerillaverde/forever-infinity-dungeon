using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopController : MonoBehaviour
{
    private BackpackController backpackPlayer1;
    private BackpackController backpackPlayer2;


    private int numPlayer;
    private GameObject player;
    private ObjectClass itemClicked;

    private void Start()
    {
        backpackPlayer1 = GameManager.Instance.GetBackpackController(1);
        backpackPlayer2 = GameManager.Instance.GetBackpackController(2);
    }

    //Funcion que se llamara al obtener un item
    public void BuyItem(ObjectClass item)
    {
        
        if (GameManager.Instance.GetPlayer(numPlayer).GetCoins() >= item.price)
        {
            if (numPlayer == 1)
            {
                if (backpackPlayer1.AddItem(item))
                {
                    GameManager.Instance.GetPlayer(numPlayer).SetGold(item.price);
                    backpackPlayer1.updateUI();
                    GameManager.Instance.SetInfoMensaje("You bought "+item.getName());
                }
                else
                    GameManager.Instance.SetInfoMensaje("You reach max stack for "+item.getName());
                

            }else
            {
                if (backpackPlayer2.AddItem(item))
                {
                    
                    GameManager.Instance.GetPlayer(numPlayer).SetGold(item.price);
                    backpackPlayer2.updateUI();
                    GameManager.Instance.SetInfoMensaje("You bought "+item.getName());
                }
                else
                    GameManager.Instance.SetInfoMensaje("You reach max stack for "+item.getName());
            }
            
        }else
            GameManager.Instance.SetInfoMensaje("You don't have enough coins");
    }

    public void setNumPlayer(int numPlayer)
    {
        this.numPlayer = numPlayer;
    }

    public int GetNumPlayer()
    {
        return numPlayer;
    }
}
