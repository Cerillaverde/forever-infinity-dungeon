using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemShop : MonoBehaviour
{
    [Header("Functionality")] 
    // [SerializeField] private GameEventItem eventClick;

    [Header("Display")] [SerializeField] private TextMeshProUGUI nametext;
    [SerializeField] private TextMeshProUGUI priceText;
    [SerializeField] private Image image;


    public void Load(ObjectClass item)
    {
        nametext.text = item.getName();
        image.sprite = item.getIcon();

        GetComponent<Button>().onClick.RemoveAllListeners();
        GetComponent<Button>().onClick.AddListener(() => RaiseEventClick(item));

    }

    public void Load(Backpack.ItemSlot itemSlot)
    {
        Load(itemSlot.item);
        
        priceText.text = itemSlot.item.price.ToString();
    }

    private void RaiseEventClick(ObjectClass item)
    {
        GameManager.Instance.audioSFXManager.BuyItemAudioPlay();
        GameManager.Instance.ClickOnItemShop(item);
    }
}