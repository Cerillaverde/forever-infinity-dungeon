using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class WallDungeonGenerator
{
    public static void CreateDungeonWalls(HashSet<Vector2Int> floorPos, VisualizeTilemap visualizeTilemap)
    {
        HashSet<Vector2Int> basicWallpos = FindWallsInDirections(floorPos, Direction2D.cardinalDirectList);
        HashSet<Vector2Int> cornerWallpos = FindWallsInDirections(floorPos, Direction2D.diagonalDirectList);

        CreateBasicWalls(visualizeTilemap, basicWallpos, floorPos);
        CreateCornerWalls(visualizeTilemap, cornerWallpos, floorPos);
    }

    private static void CreateCornerWalls(VisualizeTilemap visualizeTilemap, HashSet<Vector2Int> cornerWallpos,
        HashSet<Vector2Int> floorPos)
    {
        foreach (Vector2Int pos in cornerWallpos)
        {
            string neighboursBinaryValues = "";

            foreach (Vector2Int direction in Direction2D.eightDirectionsList)
            {
                Vector2Int neighbourPos = pos + direction;

                if (floorPos.Contains(neighbourPos))
                    neighboursBinaryValues += "1";
                else
                    neighboursBinaryValues += "0";
            }

            visualizeTilemap.PaintSingleCornerWall(pos, neighboursBinaryValues);
        }
    }

    private static void CreateBasicWalls(VisualizeTilemap visualizeTilemap, HashSet<Vector2Int> basicWallpos,
        HashSet<Vector2Int> floorPos)
    {
        foreach (Vector2Int pos in basicWallpos)
        {
            string neighboursBinaryValues = "";

            foreach (Vector2Int direction in Direction2D.cardinalDirectList)
            {
                Vector2Int neighbourPos = pos + direction;

                if (floorPos.Contains(neighbourPos))
                    neighboursBinaryValues += "1";
                else
                    neighboursBinaryValues += "0";
            }

            visualizeTilemap.PaintSingleBasicWall(pos, neighboursBinaryValues);
        }
    }

    private static HashSet<Vector2Int> FindWallsInDirections(HashSet<Vector2Int> floorPos,
        List<Vector2Int> cardinalDirectList)
    {
        HashSet<Vector2Int> wallPos = new HashSet<Vector2Int>();

        foreach (Vector2Int pos in floorPos)
        {
            foreach (Vector2Int direction in cardinalDirectList)
            {
                Vector2Int neighbourPos = pos + direction;
                if (!floorPos.Contains(neighbourPos))
                {
                    wallPos.Add(neighbourPos);
                }
            }
        }

        return wallPos;
    }
}