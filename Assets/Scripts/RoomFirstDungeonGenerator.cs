using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class RoomFirstDungeonGenerator : SimpleRandomWalkDungeon
{
    [SerializeField] private int minRoomWidth = 4;

    [SerializeField] private int minRoomHeight = 4;

    [SerializeField] private int dungeonWidth = 20;

    [SerializeField] private int dungeonHeight = 20;

    private Vector2Int lastRoomCenter;
    private Vector2Int firstRoomCenter;

    [SerializeField] [Range(0, 10)] private int offset = 1;

    [SerializeField] private bool randomWalkRooms = false;

    public GameEvent OnFinishedRoomGeneration;

    // DATA
    private DungeonData dungeonData;


    private void Awake()
    {
        dungeonData = FindObjectOfType<DungeonData>();
        if (dungeonData == null)
        {
            dungeonData = gameObject.AddComponent<DungeonData>();
        }
    }

    private void Start()
    {
        GenerateDungeon();
    }

    protected override void RunProceduralGeneration()
    {
        CreateRooms();
    }

    private void CreateRooms()
    {
        dungeonData.Reset();

        List<BoundsInt> roomList = ProceduralAlgorithms.BinarySpacePartitioning(
            new BoundsInt((Vector3Int)startPos, new Vector3Int(dungeonWidth, dungeonHeight, 0)), minRoomWidth,
            minRoomHeight);

        HashSet<Vector2Int> floor = new HashSet<Vector2Int>();

        if (randomWalkRooms)
        {
            floor = CreateRoomsRandom(roomList);
        }
        else
        {
            floor = CreateSimpleRooms(roomList);
        }


        List<Vector2Int> roomCentersPoints = new List<Vector2Int>();

        foreach (BoundsInt room in roomList)
        {
            roomCentersPoints.Add((Vector2Int)Vector3Int.RoundToInt(room.center));
        }

        List<Vector2Int> roomOrder;
        CreateRoomOrder(roomCentersPoints, out roomOrder);

        firstRoomCenter = roomOrder[0];
        lastRoomCenter = roomOrder[roomOrder.Count - 1];

        foreach (BoundsInt room in roomList)
        {
            roomCentersPoints.Add((Vector2Int)Vector3Int.RoundToInt(room.center));
        }

        HashSet<Vector2Int> corridors = ConnectRooms(roomCentersPoints);

        List<Vector2Int> corridorsList = corridors.ToList();


        // SAVE ROOM FLOOR DATA ON DungeonData
        foreach (BoundsInt room in roomList)
        {
            HashSet<Vector2Int> roomFloor = new HashSet<Vector2Int>();

            for (int col = offset; col < room.size.x - offset; col++)
            {
                for (int row = offset; row < room.size.y - offset; row++)
                {
                    Vector2Int pos = (Vector2Int)room.min + new Vector2Int(col, row);
                    roomFloor.Add(pos);
                }
            }

            dungeonData.rooms.Add(new Room((Vector2Int)Vector3Int.RoundToInt(room.center), roomFloor));
        }

        // SAVE LAST ROOM AND FIRST ROOM DATA ON DungeonData

        foreach (Room room in dungeonData.rooms)
        {
            Vector2Int roomCenter = Vector2Int.RoundToInt(room.roomCenterPos);

            if (lastRoomCenter == roomCenter)
            {
                dungeonData.lastRoom = room;
            }

            if (firstRoomCenter == roomCenter)
            {
                dungeonData.firstRoom = room;
            }
        }


        corridorsList = IncreaseCorridorBrush3by3(corridorsList);

        //corridorsList = IncreaseCorridorsSizeByOne(corridorsList);

        floor.UnionWith(corridorsList);


        // SAVE CORRIDORS FLOOR DATA ON DungeonData

        dungeonData.corridors.UnionWith(corridorsList);


        visualizeTilemap.PaintFloorTiles(floor);
        WallDungeonGenerator.CreateDungeonWalls(floor, visualizeTilemap);

        OnFinishedRoomGeneration.Raise();
    }

    private HashSet<Vector2Int> CreateRoomsRandom(List<BoundsInt> roomList)
    {
        HashSet<Vector2Int> floor = new HashSet<Vector2Int>();

        for (int i = 0; i < roomList.Count; i++)
        {
            BoundsInt roomBounds = roomList[i];
            Vector2Int roomCenter = new Vector2Int(Mathf.RoundToInt(roomBounds.center.x),
                Mathf.RoundToInt(roomBounds.center.y));
            HashSet<Vector2Int> roomFloor = RunRandomWakl(randomWalkSo, roomCenter);

            foreach (var pos in roomFloor)
            {
                if (pos.x >= (roomBounds.xMin + offset) && pos.x <= (roomBounds.xMax - offset) &&
                    pos.y >= (roomBounds.yMin + offset) && pos.y <= (roomBounds.yMax - offset))
                {
                    floor.Add(pos);
                }
            }
        }

        return floor;
    }

    private HashSet<Vector2Int> ConnectRooms(List<Vector2Int> roomCentersPoints)
    {
        HashSet<Vector2Int> corridors = new HashSet<Vector2Int>();
        Vector2Int currentRoomCenter = roomCentersPoints[Random.Range(0, roomCentersPoints.Count)];

        roomCentersPoints.Remove(currentRoomCenter);

        while (roomCentersPoints.Count() > 0)
        {
            Vector2Int closeCenter = FindClosePoint(currentRoomCenter, roomCentersPoints);
            roomCentersPoints.Remove(closeCenter);

            HashSet<Vector2Int> newCorridor = CreateCorridor(currentRoomCenter, closeCenter);
            currentRoomCenter = closeCenter;
            corridors.UnionWith(newCorridor);
        }

        return corridors;
    }

    private void CreateRoomOrder(List<Vector2Int> roomCentersPoints, out List<Vector2Int> roomCenterOrder)
    {
        Vector2Int closeCenter = Vector2Int.zero;
        Vector2Int currentRoomCenter = roomCentersPoints[Random.Range(0, roomCentersPoints.Count)];
        roomCenterOrder = new();

        roomCenterOrder.Add(currentRoomCenter);
        roomCentersPoints.Remove(currentRoomCenter);

        while (roomCentersPoints.Count() > 0)
        {
            closeCenter = FindClosePoint(currentRoomCenter, roomCentersPoints);
            roomCenterOrder.Add(closeCenter);
            roomCentersPoints.Remove(closeCenter);

            currentRoomCenter = closeCenter;
        }
    }

    private HashSet<Vector2Int> CreateCorridor(Vector2Int currentRoomCenter, Vector2Int destination)
    {
        HashSet<Vector2Int> corridor = new HashSet<Vector2Int>();

        Vector2Int pos = currentRoomCenter;

        corridor.Add(pos);

        while (pos.y != destination.y)
        {
            if (destination.y > pos.y)
            {
                pos += Vector2Int.up;
            }
            else if (destination.y < pos.y)
            {
                pos += Vector2Int.down;
            }

            corridor.Add(pos);
        }

        while (pos.x != destination.x)
        {
            if (destination.x > pos.x)
            {
                pos += Vector2Int.right;
            }
            else if (destination.x < pos.x)
            {
                pos += Vector2Int.left;
            }

            corridor.Add(pos);
        }

        return corridor;
    }

    private Vector2Int FindClosePoint(Vector2Int currentRoomCenter, List<Vector2Int> roomCentersPoints)
    {
        Vector2Int closePoint = Vector2Int.zero;
        float distance = float.MaxValue;

        foreach (var pos in roomCentersPoints)
        {
            float currentDistance = Vector2Int.Distance(pos, currentRoomCenter);

            if (currentDistance < distance)
            {
                distance = currentDistance;
                closePoint = pos;
            }
        }

        return closePoint;
    }


    private HashSet<Vector2Int> CreateSimpleRooms(List<BoundsInt> roomList)
    {
        HashSet<Vector2Int> floor = new HashSet<Vector2Int>();

        foreach (BoundsInt room in roomList)
        {
            for (int col = offset; col < room.size.x - offset; col++)
            {
                for (int row = offset; row < room.size.y - offset; row++)
                {
                    Vector2Int pos = (Vector2Int)room.min + new Vector2Int(col, row);
                    floor.Add(pos);
                }
            }
        }

        return floor;
    }

    private List<Vector2Int> IncreaseCorridorBrush3by3(List<Vector2Int> corridor)
    {
        List<Vector2Int> newCorridor = new List<Vector2Int>();

        for (int i = 1; i < corridor.Count; i++)
        {
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    newCorridor.Add(corridor[i - 1] + new Vector2Int(x, y));
                }
            }
        }

        return newCorridor;
    }

    private List<Vector2Int> IncreaseCorridorsSizeByOne(List<Vector2Int> corridor)
    {
        List<Vector2Int> newCorridor = new List<Vector2Int>();

        Vector2Int previousDirection = Vector2Int.zero;

        for (int i = 1; i < corridor.Count; i++)
        {
            Vector2Int directionFromCell = corridor[i] - corridor[i - 1];

            if (previousDirection != Vector2Int.zero && directionFromCell != previousDirection)
            {
                for (int x = -1; x < 2; x++)
                {
                    for (int y = -1; y < 2; y++)
                    {
                        newCorridor.Add(corridor[i - 1] + new Vector2Int(x, y));
                    }
                }

                previousDirection = directionFromCell;
            }
            else
            {
                previousDirection = directionFromCell;

                Vector2Int newCorridorTileOffset = GetDirection90From(directionFromCell);

                newCorridor.Add(corridor[i - 1]);
                newCorridor.Add(corridor[i - 1] + newCorridorTileOffset);
            }
        }

        return newCorridor;
    }

    private Vector2Int GetDirection90From(Vector2Int direction)
    {
        if (direction == Vector2Int.up)
        {
            return Vector2Int.right;
        }

        if (direction == Vector2Int.right)
        {
            return Vector2Int.down;
        }

        if (direction == Vector2Int.down)
        {
            return Vector2Int.left;
        }

        if (direction == Vector2Int.left)
        {
            return Vector2Int.up;
        }

        return Vector2Int.zero;
    }
}