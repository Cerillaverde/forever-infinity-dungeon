using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class AudioSFXManager : MonoBehaviour
{
    [SerializeField] private AudioClip playerHit;
    [SerializeField] private AudioClip buyItem;
    [SerializeField] private AudioClip heal;
    [SerializeField] private AudioClip noHeal;

    [SerializeField] private SOGeneralOptions SOPersistent;

    private AudioSource audioSource;

    private void OnEnable()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void SetVolume()
    {
        audioSource.volume = SOPersistent.audioSFXVolume;
    }

    public void PlayerHitAudioPlay()
    {
        audioSource.clip = playerHit;
        audioSource.Play();
    }

    public void BuyItemAudioPlay()
    {
        audioSource.clip = buyItem;
        audioSource.Play();
    }

    public void HealAudioPlay()
    {
        audioSource.clip = heal;
        audioSource.Play();
    }

    public void NegativeHealAudioPlay()
    {
        audioSource.clip = noHeal;
        audioSource.Play();
    }
}


