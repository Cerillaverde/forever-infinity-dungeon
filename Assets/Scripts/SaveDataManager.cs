using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveDataManager : MonoBehaviour
{
 private const string saveFileName = "savegame.json";

        public void SaveData()
        {
            ISaveableObject[] saveableObjects = FindObjectsByType<PlayerController>(FindObjectsSortMode.None);
            SaveData data = new SaveData();
            data.PopulateData(saveableObjects);
            string jsonData = JsonUtility.ToJson(data);

            try
            {
                Debug.Log("Saving: ");
                Debug.Log(jsonData);

                File.WriteAllText(saveFileName, jsonData);
            }
            catch (Exception e)
            {
                Debug.LogError($"Error while trying to save {Path.Combine(Application.persistentDataPath, saveFileName)} with exception {e}");
            }
        }

        public void LoadData()
        {
            try
            {
                string jsonData = File.ReadAllText(saveFileName);

                SaveData data = new SaveData();
                JsonUtility.FromJsonOverwrite(jsonData, data);

                foreach (SaveData.PlayerData playerdata in data.Players)
                {
                    if(playerdata.playerNumber == 1)
                        GameManager.Instance.FromSaveDataPlayer1(playerdata);
                    if(playerdata.playerNumber == 2)
                        GameManager.Instance.FromSaveDataPlayer2(playerdata);
                    GameManager.Instance.Round = playerdata.round;
                }

                GameManager.Instance.Round--;
                GameManager.Instance.FirstTime = false;
                GameManager.Instance.LoadShopScene();
            }
            catch (Exception e)
            {
                Debug.LogError($"Error while trying to load {Path.Combine(Application.persistentDataPath, saveFileName)} with exception {e}");
            }
        }

}
