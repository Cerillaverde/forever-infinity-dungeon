using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.UI;

public class EventForInputVirtualMouse : InputSystemUIInputModule
{
    // Start is called before the first frame update
    public override void ActivateModule()
    {
        gameObject.SetActive(true);
    }

    public override void DeactivateModule()
    {
        gameObject.SetActive(false);
    }
}
