using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class AudioBGManager : MonoBehaviour
{
    [SerializeField] private AudioClip menuBackground;
    [SerializeField] private AudioClip[] shopBackground;
    [SerializeField] private AudioClip[] dungeonBackground;
    [SerializeField] private AudioClip[] bossBackground;
    [SerializeField] private AudioClip gameOverBackground;

    [SerializeField] private SOGeneralOptions SOPersistent;

    private AudioSource audioSource;

    private void OnEnable()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void SetVolume()
    {
        audioSource.volume = SOPersistent.audioBGVolume;
    }

    public void MenuBackgroundAudioPlay()
    {
        audioSource.clip = menuBackground;
        audioSource.Play();
    }
    
    public void ShopBackgroundAudioPlay()
    {
        audioSource.clip = shopBackground[Random.Range(0, bossBackground.Length)];
        audioSource.Play();
    }

    public void DungeonBackgroundAudioPlay()
    {
        audioSource.clip = dungeonBackground[Random.Range(0, bossBackground.Length)];
        audioSource.Play();
    }
    
    public void BossBackgroundAudioPlay()
    {
        audioSource.clip = bossBackground[Random.Range(0, bossBackground.Length)];
        audioSource.Play();
    }

    public void GameOverBackgroundAudioPlay()
    {
        audioSource.clip = gameOverBackground;
        audioSource.Play();
    }

}
