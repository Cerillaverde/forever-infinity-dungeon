using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[Serializable]
public class SaveData
{
    [Serializable]
    public struct PlayerData
    {
        public ArmorObject armorPlayer;
        public WeaponObject weaponPlayer;
        public int playerLevel;
        public float experienceRequired;
        public float experience;
        public int gold;
        public int playerNumber;
        public float speed;
        public float HpMax;
        public float HpActual;
        public float damage;
        public float defense;
        public bool ricochetActive;
        public bool explosionActive;
        public int round;

        public PlayerData(SOPlayer so)
        {
            armorPlayer = so.armorPlayer;
            weaponPlayer = so.weaponPlayer;
            playerLevel = so.playerLevel;
            playerNumber = so.playerNumber;
            experienceRequired = so.experienceRequired;
            experience = so.experience;
            gold = so.gold;
            speed = so.speed;
            HpMax = so.maxHp;
            HpActual = so.hp;
            damage = so.attack;
            defense = so.defense;
            ricochetActive = so.abilityRicochet;
            explosionActive = so.abilityExplosion;
            round = so.round;
        }
    }

    public PlayerData[] Players;

    public void PopulateData(ISaveableObject[] playersData)
    {
        Players = new PlayerData[playersData.Length];
        for (int i = 0; i < playersData.Length; i++)
            Players[i] = playersData[i].Save();
    }
}

public interface ISaveableObject
{
    public SaveData.PlayerData Save();
    public void Load(SaveData.PlayerData playerData);
}
