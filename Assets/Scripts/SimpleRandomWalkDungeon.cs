using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class SimpleRandomWalkDungeon : AbstractDungeonGenerator
{
    [SerializeField] protected SimpleRandomWalkSO randomWalkSo;

    public GameEvent OnFinishedBossRoomGeneration;

    // DATA
    private DungeonData dungeonData;


    private void Awake()
    {
        dungeonData = FindObjectOfType<DungeonData>();
        if (dungeonData == null)
            dungeonData = gameObject.AddComponent<DungeonData>();
    }

    private void Start()
    {
        GenerateDungeon();
    }


    protected override void RunProceduralGeneration()
    {
        HashSet<Vector2Int> floorPos = RunRandomWakl(randomWalkSo, startPos);

        dungeonData.startPosRandomWalk = startPos;
        dungeonData.floorRandomWalk = floorPos;

        visualizeTilemap.Clear();

        visualizeTilemap.PaintFloorTiles(floorPos);
        WallDungeonGenerator.CreateDungeonWalls(floorPos, visualizeTilemap);

        OnFinishedBossRoomGeneration.Raise();
    }

    protected HashSet<Vector2Int> RunRandomWakl(SimpleRandomWalkSO param, Vector2Int pos)
    {
        Vector2Int currentPos = pos;

        HashSet<Vector2Int> floorPos = new HashSet<Vector2Int>();

        for (int i = 0; i < param.iteractionsNumber; i++)
        {
            HashSet<Vector2Int> path = ProceduralAlgorithms.simpleRandomWalk(currentPos, param.walkLenght);
            floorPos.UnionWith(path);

            if (param.startRandomIteration)
                currentPos = floorPos.ElementAt(Random.Range(0, floorPos.Count));
        }

        return floorPos;
    }
}