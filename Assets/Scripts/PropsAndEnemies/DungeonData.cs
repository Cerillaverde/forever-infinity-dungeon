using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonData : MonoBehaviour
{
    public List<Room> rooms { get; set; } = new List<Room>();
    public HashSet<Vector2Int> corridors { get; set; } = new HashSet<Vector2Int>();

    public Vector2Int startPosRandomWalk;
    public HashSet<Vector2Int> floorRandomWalk;

    public Room lastRoom;
    public Room firstRoom;

    public GameObject Player1Reference { get; set; }
    public GameObject Player2Reference { get; set; }

    public void Reset()
    {
        foreach (Room room in rooms)
        {
            foreach (GameObject item in room.propObjectReferences)
            {
                Destroy(item);
            }

            foreach (GameObject item in room.enemiesInTheRoom)
            {
                Destroy(item);
            }
        }

        rooms = new List<Room>();
        corridors = new HashSet<Vector2Int>();
        Destroy(Player1Reference);
        Destroy(Player2Reference);
    }
}

public class Room
{
    public Vector2 roomCenterPos { get; set; }
    public HashSet<Vector2Int> floorTiles { get; private set; } = new HashSet<Vector2Int>();

    public HashSet<Vector2Int> nearWallTilesUp { get; set; } = new HashSet<Vector2Int>();
    public HashSet<Vector2Int> nearWallTilesDown { get; set; } = new HashSet<Vector2Int>();
    public HashSet<Vector2Int> nearWallTilesLeft { get; set; } = new HashSet<Vector2Int>();
    public HashSet<Vector2Int> nearWallTilesRight { get; set; } = new HashSet<Vector2Int>();
    public HashSet<Vector2Int> cornerTiles { get; set; } = new HashSet<Vector2Int>();
    public HashSet<Vector2Int> innerTiles { get; set; } = new HashSet<Vector2Int>();

    public HashSet<Vector2Int> propPositions { get; set; } = new HashSet<Vector2Int>();
    public List<GameObject> propObjectReferences { get; set; } = new List<GameObject>();

    public List<Vector2Int> positionsAccessibleFromPath { get; set; } = new List<Vector2Int>();

    public List<GameObject> enemiesInTheRoom { get; set; } = new List<GameObject>();

    public Room(Vector2 roomCenterPos, HashSet<Vector2Int> floorTiles)
    {
        this.roomCenterPos = roomCenterPos;
        this.floorTiles = floorTiles;
    }
}