using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NavMeshPlus.Components;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class PropPlacementManager : MonoBehaviour
{
    DungeonData dungeonData;

    [SerializeField] private List<PropSO> propsToPlace;

    [SerializeField, Range(0, 1)] private float cornerPropPlacementChance = 0.5f;

    [SerializeField] private GameObject propPrefab;

    public GameEvent OnFinished;

    private Color lightColor = Color.yellow;

    private void Awake()
    {
        dungeonData = FindObjectOfType<DungeonData>();
    }

    public void ProcessRooms()
    {
        if (dungeonData == null)
        {
            return;
        }

        foreach (Room room in dungeonData.rooms)
        {
            List<PropSO> cornerProps = propsToPlace.Where(x => x.Corner).ToList();
            PlaceCornerProps(room, cornerProps);

            List<PropSO> leftWallProps = propsToPlace.Where(x => x.NearWallLeft)
                .OrderByDescending(x => x.PropSize.x * x.PropSize.y).ToList();

            PlaceProps(room, leftWallProps, room.nearWallTilesLeft, PlacementOriginCorner.BottomLeft);

            List<PropSO> rightWallProps = propsToPlace.Where(x => x.NearWallRight)
                .OrderByDescending(x => x.PropSize.x * x.PropSize.y).ToList();

            PlaceProps(room, rightWallProps, room.nearWallTilesRight, PlacementOriginCorner.TopRight);

            List<PropSO> topWallProps = propsToPlace.Where(x => x.NearWallUP)
                .OrderByDescending(x => x.PropSize.x * x.PropSize.y).ToList();

            PlaceProps(room, topWallProps, room.nearWallTilesUp, PlacementOriginCorner.TopLeft);

            List<PropSO> downWallProps = propsToPlace.Where(x => x.NearWallDown)
                .OrderByDescending(x => x.PropSize.x * x.PropSize.y).ToList();

            PlaceProps(room, downWallProps, room.nearWallTilesDown, PlacementOriginCorner.BottomLeft);

            List<PropSO> innerProps = propsToPlace.Where(x => x.Inner)
                .OrderByDescending(x => x.PropSize.x * x.PropSize.y).ToList();

            PlaceProps(room, innerProps, room.innerTiles, PlacementOriginCorner.BottomLeft);

            List<PropSO> bossFightProp = propsToPlace.Where(x => x.LastRoom).ToList();

            PropSO lastRoomProp = bossFightProp.First();

            Vector2Int lastRoomCenter = Vector2Int.RoundToInt(dungeonData.lastRoom.roomCenterPos);

            PlacePropGameObjectAt(dungeonData.lastRoom, lastRoomCenter, lastRoomProp);
        }

        OnFinished.Raise();
    }

    private void PlaceProps(Room room, List<PropSO> wallProps, HashSet<Vector2Int> availableTiles,
        PlacementOriginCorner placement)
    {
        HashSet<Vector2Int> tempPositons = availableTiles;
        tempPositons.ExceptWith(dungeonData.corridors);
        tempPositons.ExceptWith(room.propPositions);

        foreach (PropSO propToPlace in wallProps)
        {
            int quantity =
                UnityEngine.Random.Range(propToPlace.PlacementQuantityMin, propToPlace.PlacementQuantityMax + 1);

            for (int i = 0; i < quantity; i++)
            {
                tempPositons.ExceptWith(room.propPositions);
                List<Vector2Int> availablePositions = tempPositons.OrderBy(x => Guid.NewGuid()).ToList();

                if (TryPlacingPropBruteForce(room, propToPlace, availablePositions, placement) == false)
                    break;
            }
        }
    }

    private bool TryPlacingPropBruteForce(Room room, PropSO propToPlace, List<Vector2Int> availablePositions,
        PlacementOriginCorner placement)
    {
        for (int i = 0; i < availablePositions.Count; i++)
        {
            Vector2Int position = availablePositions[i];
            if (room.propPositions.Contains(position))
                continue;

            List<Vector2Int> freePositionsAround = TryToFitProp(propToPlace, availablePositions, position, placement);

            if (freePositionsAround.Count == propToPlace.PropSize.x * propToPlace.PropSize.y)
            {
                PlacePropGameObjectAt(room, position, propToPlace);
                foreach (Vector2Int pos in freePositionsAround)
                {
                    room.propPositions.Add(pos);
                }

                if (propToPlace.PlaceAsGroup)
                    PlaceGroupObject(room, position, propToPlace, 1);

                return true;
            }
        }

        return false;
    }

    private List<Vector2Int> TryToFitProp(PropSO prop, List<Vector2Int> availablePositions, Vector2Int originPosition,
        PlacementOriginCorner placement)
    {
        List<Vector2Int> freePositions = new List<Vector2Int>();

        if (placement == PlacementOriginCorner.BottomLeft)
        {
            for (int xOffset = 0; xOffset < prop.PropSize.x; xOffset++)
            {
                for (int yOffset = 0; yOffset < prop.PropSize.y; yOffset++)
                {
                    Vector2Int tempPos = originPosition + new Vector2Int(xOffset, yOffset);
                    if (availablePositions.Contains(tempPos))
                    {
                        freePositions.Add(tempPos);
                    }
                }
            }
        }

        else if (placement == PlacementOriginCorner.BottomRight)
        {
            for (int xOffset = -prop.PropSize.x + 1; xOffset <= 0; xOffset++)
            {
                for (int yOffset = 0; yOffset < prop.PropSize.y; yOffset++)
                {
                    Vector2Int tempPos = originPosition + new Vector2Int(xOffset, yOffset);
                    if (availablePositions.Contains(tempPos))
                    {
                        freePositions.Add(tempPos);
                    }
                }
            }
        }
        else if (placement == PlacementOriginCorner.TopLeft)
        {
            for (int xOffset = 0; xOffset < prop.PropSize.x; xOffset++)
            {
                for (int yOffset = -prop.PropSize.y + 1; yOffset <= 0; yOffset++)
                {
                    Vector2Int tempPos = originPosition + new Vector2Int(xOffset, yOffset);
                    if (availablePositions.Contains(tempPos))
                    {
                        freePositions.Add(tempPos);
                    }
                }
            }
        }
        else
        {
            for (int xOffset = -prop.PropSize.x + 1; xOffset <= 0; xOffset++)
            {
                for (int yOffset = -prop.PropSize.y + 1; yOffset <= 0; yOffset++)
                {
                    Vector2Int tempPos = originPosition + new Vector2Int(xOffset, yOffset);
                    if (availablePositions.Contains(tempPos))
                    {
                        freePositions.Add(tempPos);
                    }
                }
            }
        }

        return freePositions;
    }

    private void PlaceCornerProps(Room room, List<PropSO> cornerProps)
    {
        float tempChance = cornerPropPlacementChance;

        foreach (Vector2Int cornerTile in room.cornerTiles)
        {
            if (UnityEngine.Random.value < tempChance)
            {
                PropSO propToPlace = cornerProps[UnityEngine.Random.Range(0, cornerProps.Count)];

                PlacePropGameObjectAt(room, cornerTile, propToPlace);

                if (propToPlace.PlaceAsGroup)
                {
                    PlaceGroupObject(room, cornerTile, propToPlace, 2);
                }
            }
            else
            {
                tempChance = Mathf.Clamp01(tempChance + 0.1f);
            }
        }
    }

    private void PlaceGroupObject(Room room, Vector2Int groupOriginPosition, PropSO propToPlace, int searchOffset)
    {
        int count = UnityEngine.Random.Range(propToPlace.GroupMinCount, propToPlace.GroupMaxCount) - 1;
        count = Mathf.Clamp(count, 0, 8);

        List<Vector2Int> availableSpaces = new List<Vector2Int>();

        for (int xOffset = -searchOffset; xOffset <= searchOffset; xOffset++)
        {
            for (int yOffset = -searchOffset; yOffset <= searchOffset; yOffset++)
            {
                Vector2Int tempPos = groupOriginPosition + new Vector2Int(xOffset, yOffset);
                if (room.floorTiles.Contains(tempPos) && !dungeonData.corridors.Contains(tempPos) &&
                    !room.propPositions.Contains(tempPos))
                {
                    availableSpaces.Add(tempPos);
                }
            }
        }

        availableSpaces.OrderBy(x => Guid.NewGuid());

        int tempCount = count < availableSpaces.Count ? count : availableSpaces.Count;

        for (int i = 0; i < tempCount; i++)
        {
            if (!room.propPositions.Contains(availableSpaces[i]))
            {
                PlacePropGameObjectAt(room, availableSpaces[i], propToPlace);
            }
        }
    }

    private void PlacePropGameObjectAt(Room room, Vector2Int placementPostion, PropSO propToPlace)
    {
        if (!room.propPositions.Contains(placementPostion)
            && !dungeonData.corridors.Contains(placementPostion + Vector2Int.up)
            && !dungeonData.corridors.Contains(placementPostion + Vector2Int.down)
            && !dungeonData.corridors.Contains(placementPostion + Vector2Int.right)
            && !dungeonData.corridors.Contains(placementPostion + Vector2Int.left)
            || propToPlace.LastRoom
            && !room.propPositions.Contains(placementPostion))
        {
            GameObject prop = Instantiate(propPrefab);
            if (propToPlace.LastRoom)
            {
                prop.name = "LastRoom";
                prop.transform.GetChild(0).tag = "LastRoomTag";
            }


            SpriteRenderer propSpriteRenderer = prop.GetComponentInChildren<SpriteRenderer>();


            if (propToPlace.Light)
            {
                Light2D light = propSpriteRenderer.gameObject.AddComponent<Light2D>();

                light.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));

                light.intensity = 0.2f;

                light.pointLightInnerRadius = 6f;
                light.pointLightOuterRadius = 8f;
            }

            propSpriteRenderer.sprite = propToPlace.PropSprite;

            CapsuleCollider2D collider = propSpriteRenderer.gameObject.AddComponent<CapsuleCollider2D>();
            collider.offset = Vector2.zero;
            if (propToPlace.PropSize.x > propToPlace.PropSize.y)
            {
                collider.direction = CapsuleDirection2D.Horizontal;
            }

            Vector2 size = new Vector2(propToPlace.PropSize.x * 0.8f, propToPlace.PropSize.y * 0.8f);
            collider.size = size;

            if (propToPlace.LastRoom)
            {
                collider.isTrigger = true;
            }


            prop.transform.localPosition = (Vector2)placementPostion;

            propSpriteRenderer.transform.localPosition = (Vector2)propToPlace.PropSize * 0.5f;

            if (!propToPlace.Effector && !propToPlace.LastRoom)
            {
                NavMeshObstacle obstacle = propSpriteRenderer.gameObject.AddComponent<NavMeshObstacle>();

                obstacle.carving = true;
            }

            room.propPositions.Add(placementPostion);
            room.propObjectReferences.Add(prop);
        }
    }
}

public enum PlacementOriginCorner
{
    BottomLeft,
    BottomRight,
    TopLeft,
    TopRight
}