using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyControl : MonoBehaviour
{
    [SerializeField] protected LayerMask layerMask;
    [SerializeField] protected GameObject enemyHurtbox;
    [SerializeField] protected GameObject enemyHitbox;
    // protected SOEnemy statsEnemy;

    protected float experienceMin;
    protected float experienceMax;
    protected int goldMax;
    protected int goldMin;

    protected float hpMax;
    protected float hpActual;
    protected float moveSpeed;
    protected float damage;
    protected float rangeAttack;
    protected float rangeDetection;
    protected Color colorOfEnemy;

    protected bool playerTraked;

    protected UnityEngine.AI.NavMeshAgent agent;

    protected bool canAttack = true;
    protected bool inmune = false;
    protected Collider2D playerCollider;

    protected Vector2 originPosition;

    protected enum SwitchMachineStates
    {
        NONE,
        IDLE,
        WALK,
        ATTACK,
        RECIVEHIT,
        DIE
    };

    protected SwitchMachineStates currentState;
    protected SwitchMachineStates lastState;

    public abstract void OnHit(float damage);
}