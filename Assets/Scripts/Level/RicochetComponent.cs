using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RicochetComponent : MonoBehaviour
{
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private SOAbility soAbility;
    private float speed;
    private float damageAdaptable;
    private float timeOfCooldown;
    private float rangeDetection;
    private float damageAdaptableDrop;

    private Collider2D[] target;
    private List<Collider2D> colliders;
    private List<Collider2D> collidersDamageDone = new List<Collider2D>();
    private Collider2D actualTarget;
    private PlayerController player;

    private void Awake()
    {
        speed = soAbility.abilityAdvanceSpeed;
        damageAdaptable = soAbility.damageAdaptable;
        timeOfCooldown = soAbility.timeOfCooldown;
        rangeDetection = soAbility.rangeDetection;
        damageAdaptableDrop = soAbility.damageAdaptableDrop;
    }

    private void LateUpdate()
    {
        float angle = Mathf.Atan2(actualTarget.transform.position.y - transform.position.y,
            actualTarget.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
        
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));

        GetComponent<Rigidbody2D>().velocity =
            (actualTarget.transform.position - transform.position).normalized * speed;
    }

    public void ShootRicochet(PlayerController playerController)
    {
        player = playerController;
        damageAdaptable = soAbility.damageAdaptable;
        player.StartCooldownAbility1(timeOfCooldown);
        MoveToNextTarget();
    }


    public void MoveToNextTarget()
    {
        GetComponent<HitboxInfo>().SetDamage(player.GetDamage() * (damageAdaptable / 100 + 1));

        target = Physics2D.OverlapCircleAll(transform.position, rangeDetection, layerMask);
        colliders = target.ToList();
        
        if (colliders.Count <= 0)
            Destroy(gameObject);

        colliders.Sort((collider1, collider2) =>
        {
            float distance1 = Vector2.Distance(collider1.gameObject.transform.position, transform.position);
            float distance2 = Vector2.Distance(collider2.gameObject.transform.position, transform.position);
            return distance1.CompareTo(distance2);
        });

        foreach (Collider2D newTarget in colliders)
        {
            if (!collidersDamageDone.Contains(newTarget))
            {
                collidersDamageDone.Add(newTarget);
                actualTarget = newTarget;
                damageAdaptable -= damageAdaptableDrop;
                return;
            }
        }

        collidersDamageDone.Clear();
        Destroy(gameObject);
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision == actualTarget)
            MoveToNextTarget();
    }
}