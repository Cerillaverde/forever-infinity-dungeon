using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Backpack", menuName = "Inventory/Backpack")]
public class Backpack : ScriptableObject
{
    [Serializable]
    public class ItemSlot
    {
        [SerializeField] public ObjectClass item;
        [SerializeField] public int amount;
        [HideInInspector] public int price;


        public ItemSlot(ObjectClass obj)
        {
            item = obj;
            amount = 1;
            price = item.price;
        }
    }

    [SerializeField] private List<ItemSlot> m_ItemSlots = new List<ItemSlot>();
    public ReadOnlyCollection<ItemSlot> ItemSlots => new ReadOnlyCollection<ItemSlot>(m_ItemSlots);

    public void AddItem(ObjectClass usedItem)
    {
        ItemSlot item = GetItem(usedItem);

        if (item == null)
        {
            m_ItemSlots.Add(new ItemSlot(usedItem));
        }

        else
        {
           
        }
    }

    public void AddItemStackeable(ObjectClass usedItem)
    {
        ItemSlot item = GetItem(usedItem);
        IStackable istack = item.item as IStackable;
        if (item == null || istack.GetMaxStack() == item.amount)
            m_ItemSlots.Add(new ItemSlot(usedItem));
        else
            item.amount++;
        
    }

    public void RemoveItem(ObjectClass usedItem)
    {
        ItemSlot item = GetItem(usedItem);
        if (item == null)
            return;

        item.amount--;
        if (item.amount <= 0)
            m_ItemSlots.Remove(item);
    }

    public ItemSlot GetItem(ObjectClass item)
    {
        return m_ItemSlots.FirstOrDefault(slot => slot.item == item);
    }

    public bool IncrementAmount(ObjectClass itemToIncrement)
    {
        ItemSlot slot = getSlot(itemToIncrement);
        slot.amount++;
        return true;
    }

    public ItemSlot getSlot(ObjectClass item)
    {
        foreach (ItemSlot slot in m_ItemSlots)
        {
            if (slot.item.Equals(item))
            {
                return slot;
            }
        }

        return null;
    }

    public int GetSize()
    {
        return m_ItemSlots.Count;
    }

    public bool ContainsItem(ObjectClass item)
    {
        foreach (ItemSlot slot in m_ItemSlots)
        {
            if (slot.item.Equals(item))
            {
                return true;
            }
        }

        return false;
    }

    public void AddItemSlot(ItemSlot slotToAdd)
    {
        m_ItemSlots.Add(slotToAdd);
    }

    public void DeEquipAllItems()
    {
        foreach (ItemSlot items in m_ItemSlots)
        {
            ObjectClass i = items.item;
            if (i is IEquipable)
            {
                IEquipable ie = i as IEquipable;
                ie.isEquiped(false);
            }

            if (i.fastAcces)
            {
                i.fastAcces = false;
            }
        }
    }

    public List<ItemSlot> GetItems()
    {
        return m_ItemSlots;
    }

    public void ClearBackPack()
    {
        m_ItemSlots.Clear();
    }
}