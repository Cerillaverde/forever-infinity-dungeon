using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour, IHealable, ISaveableObject
{
    [SerializeField] private GameObject playerHurtbox;
    [SerializeField] private GameObject playerHitbox;
    [SerializeField] private GameObject ricochetPrefab;
    [SerializeField] private GameObject boomerangPrefab;
    [SerializeField] private GameObject repelPrefab;
    [SerializeField] private GameObject explosionPrefab;
    [SerializeField] private GameObject directionPreviewPrefab;
    [SerializeField] private ArmorObject armor;
    [SerializeField] private WeaponObject weapon;

    private PlayerInput playerInput;
    private Vector2 movement;
    private Animator animator;
    private Rigidbody2D playerRigidbody;


    [Space(10)] [Header("Stats of player")] 
    [SerializeField] private SOPlayer playerSo;
    [SerializeField] private SOPlayer currentPlayerSo;

    private float experienceRequired;
    private float experience;
    private int gold;
    private int playerNumber;
    private float speed;
    private float HpMax;
    private float HpActual;
    private float damage;
    private float defense;
    private bool ricochetActive;
    private bool explosionActive;

    [Space(10)] [Header("Memory var")] 
    private int playerLevel;
    private bool iCanDash;
    private Vector2 savedVelocity;
    private bool ability1IsUsable = true;
    private bool ability2IsUsable = true;
    private bool inAttack = false;

    private GameObject directionPreview = null;

    private enum SwitchMachineStates
    {
        NONE,
        IDLE,
        WALK,
        DASH,
        ATTACK,
        ABILITY2,
        RECIVEHIT,
        DIE
    };

    private SwitchMachineStates currentState;
    private SwitchMachineStates lastState;

    void Start()
    {
        playerInput = GetComponent<PlayerInput>();
        playerRigidbody = GetComponent<Rigidbody2D>();
        iCanDash = true;
        
        playerNumber = playerSo.playerNumber;
        
        if (GameManager.Instance.FirstTime)
            SetupScriptableObject();
        if (!GameManager.Instance.FirstTime)
            LoadCurrentPlayer();

        if (GameManager.Instance.DeadButContinue)
        {
            SetupScriptableObject();
            gold = currentPlayerSo.gold/2;
        }
        
        UpdateAllUI();

        lastState = SwitchMachineStates.IDLE;
        InitState(SwitchMachineStates.IDLE);
    }

    void Update()
    {
        UpdateState();
        movement = playerInput.actions["Move"].ReadValue<Vector2>();
        
    }

    public void PauseGame(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            if (!GameManager.Instance.GetGameIsPaused())
                GameManager.Instance.PauseGame(true);
            else
                GameManager.Instance.ResumeGame();
        }
    }

    private void UpdateAllUI()
    {
        GameManager.Instance.SetAbilitiesCanvas(ricochetActive, explosionActive, playerNumber);
        GameManager.Instance.SetPlayer(this);
        GameManager.Instance.SetMaxHealtBar(HpMax, playerNumber);
        GameManager.Instance.SetHealtBar(HpActual, playerNumber);
        GameManager.Instance.SetGoldCanvas(gold, playerNumber);        
        GameManager.Instance.SetExprienceMaxBar(experienceRequired, playerNumber);
        GameManager.Instance.SetExprienceBar(experience, playerNumber);
        GameManager.Instance.SetPlayerLevelCanvas(playerLevel, playerNumber);
        UpdateArmor();
        UpdateWeapon();
    }

    private void SetupScriptableObject()
    {
        playerLevel = 1;
        armor = playerSo.armorPlayer;
        weapon = playerSo.weaponPlayer;
        experienceRequired = playerSo.experienceRequired;
        experience = 0;
        gold = playerSo.gold;
        HpMax = playerSo.maxHp;
        HpActual = HpMax;
        playerNumber = playerSo.playerNumber;
        speed = playerSo.speed;
        damage = playerSo.attack;
        defense = playerSo.defense;

        ricochetActive = playerSo.abilityRicochet;
        explosionActive = playerSo.abilityExplosion;
    }

    //********************** Machine States **********************

    #region SwitchMachineStates

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == currentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        this.currentState = currentState;
        switch (this.currentState)
        {
            case SwitchMachineStates.IDLE:

                playerRigidbody.velocity = Vector2.zero;

                break;


            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.DASH:

                break;

            case SwitchMachineStates.ATTACK:
                playerRigidbody.velocity = Vector2.zero;
                inAttack = true;
                playerHitbox.GetComponent<HitboxInfo>().SetDamage(damage);
                playerHitbox.SetActive(true);
                StartCoroutine(NormalAttackCooldown(0.3f));

                break;

            case SwitchMachineStates.ABILITY2:
                playerRigidbody.velocity = Vector2.zero;
                break;

            case SwitchMachineStates.RECIVEHIT:
                break;

            case SwitchMachineStates.DIE:
                SaveCurrentPlayer();
                GameManager.Instance.LoadGameOverScene();
                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.DASH:
                playerHurtbox.SetActive(true);
                break;

            case SwitchMachineStates.ATTACK:
                playerHitbox.SetActive(false);
                break;

            case SwitchMachineStates.ABILITY2:
                break;

            case SwitchMachineStates.RECIVEHIT:
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                if (movement != Vector2.zero && !inAttack)
                    ChangeState(SwitchMachineStates.WALK);

                break;

            case SwitchMachineStates.WALK:

                playerRigidbody.velocity = movement * speed;

                if (!inAttack)
                {
                    if (playerRigidbody.velocity == Vector2.zero)
                        ChangeState(SwitchMachineStates.IDLE);
                    if (playerRigidbody.velocity.y > 0 && playerRigidbody.velocity.x == 0)
                        transform.eulerAngles = new Vector3(0, 0, 0);
                    if (playerRigidbody.velocity.y < 0 && playerRigidbody.velocity.x == 0)
                        transform.eulerAngles = new Vector3(0, 0, 180);
                    if (playerRigidbody.velocity.x < 0 && playerRigidbody.velocity.y == 0)
                        transform.eulerAngles = new Vector3(0, 0, 90);
                    if (playerRigidbody.velocity.x > 0 && playerRigidbody.velocity.y == 0)
                        transform.eulerAngles = new Vector3(0, 0, 270);
                }

                lastState = currentState;
                break;

            case SwitchMachineStates.DASH:
                break;

            case SwitchMachineStates.ATTACK:
                break;

            default:
                break;
        }
    }

    public void EndAnimation()
    {
        ChangeState(lastState);
    }

    #endregion


    //********************** ABILITIES **********************
    public void AttackAction(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            GameManager.Instance.audioSFXManager.PlayerHitAudioPlay();
            ChangeState(SwitchMachineStates.ATTACK);
        }
    }
    
    // ------ DASH -----

    #region Dash

    public void Dash(InputAction.CallbackContext callbackContext)
    {
        if (iCanDash && callbackContext.performed)
        {
            ChangeState(SwitchMachineStates.DASH);
            playerHurtbox.SetActive(false);
            savedVelocity = playerRigidbody.velocity;
            playerRigidbody.velocity = playerRigidbody.velocity.normalized * 20;
            StartCoroutine(DashInAction());
            StartCoroutine(CooldownDash());
        }
    }

    #endregion

    // ------ Ability 1 ----

    #region Ability 1

    public void Ability1Action(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed && ability1IsUsable)
        {
            if (playerSo.abilityRicochet)
            {
                GameObject ricochet = Instantiate(ricochetPrefab);
                ricochet.transform.position = transform.position;
                ricochet.GetComponent<RicochetComponent>().ShootRicochet(this);
            }
            else if (playerSo.abilityBoomerang)
            {
                directionPreview = Instantiate(directionPreviewPrefab);
                directionPreview.transform.position = transform.position;
                directionPreview.GetComponent<DirectionBoomerangPreview>().VisualizeToShoot(this);
            }
        }

        if (callbackContext.canceled)
        {
            if (playerSo.abilityBoomerang && ability1IsUsable)
            {
                if (directionPreview != null)
                    directionPreview.GetComponent<DirectionBoomerangPreview>().PreviewDestroyHimself();
                GameObject boomerang = Instantiate(boomerangPrefab);
                boomerang.transform.position = transform.position;
                boomerang.GetComponent<BoomerangComponent>().ShootWhereVisualize(this);
            }

            ability1IsUsable = false;
        }
    }

    public void StartCooldownAbility1(float timeOfCooldown)
    {
        StartCoroutine(CooldownAbility1(timeOfCooldown));
    }

    #endregion

    // ------ Ability 2 ----

    #region Ability 2

    public void Ability2Action(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed && ability2IsUsable)
        {
            ability2IsUsable = false;
            if (playerSo.abilityExplosion)
            {
                GameObject explosion = Instantiate(explosionPrefab);
                explosion.GetComponent<ExplosionComponent>().ExplosionActive(this);
            }
            else if (playerSo.abilityRepel)
            {
                GameObject repel = Instantiate(repelPrefab);
                repel.GetComponent<RepelComponent>().RepelEnemies(this);
            }
        }
    }

    public void StartCooldownAbility2(float timeOfCooldown)
    {
        StartCoroutine(CooldownAbility2(timeOfCooldown));
    }

    public void EndAbility2()
    {
        ChangeState(lastState);
    }

    #endregion

    //***************** COURUTINES ******************

    #region Courutines

    IEnumerator TimeOfInvulnerability()
    {
        yield return new WaitForSeconds(0.4f);
        GetComponent<SpriteRenderer>().color = Color.white;
        playerHurtbox.SetActive(true);
    }
    
    IEnumerator NormalAttackCooldown(float f)
    {
        yield return new WaitForSeconds(f);
        ChangeState(lastState);
        inAttack = false;
    }

    IEnumerator CooldownDash()
    {
        iCanDash = false;
        yield return new WaitForSeconds(1.5f);
        iCanDash = true;
    }

    IEnumerator DashInAction()
    {
        yield return new WaitForSeconds(0.2f);
        playerRigidbody.velocity = savedVelocity;
        ChangeState(lastState);
    }

    private IEnumerator CooldownAbility1(float timeOfCooldown)
    {
        GameManager.Instance.Ability1CooldownCanvas(this, timeOfCooldown);
        yield return new WaitForSeconds(timeOfCooldown);
        ability1IsUsable = true;
    }

    private IEnumerator CooldownAbility2(float timeOfCooldown)
    {
        GameManager.Instance.Ability2CooldownCanvas(this, timeOfCooldown);
        yield return new WaitForSeconds(timeOfCooldown);
        ability2IsUsable = true;
    }

    #endregion

    //**************** GETTERS/SETTERS ******************

    #region Getters Setters

    public float GetDamage()
    {
        return damage;
    }
    public int GetPlayer()
    {
        return playerNumber;
    }

    public int GetCoins()
    {
        return gold;
    }

    public void SetGold(int gold)
    {
        this.gold = (this.gold - gold);
        GameManager.Instance.SetGoldCanvas(this.gold, playerNumber);
    }

    #endregion
    
    
    // *************** UNDEFINIED ***************

    #region Variety

    public void OnHit(float damage)
    {
        if (playerHurtbox.activeSelf)
        {
            playerHurtbox.SetActive(false);
            GetComponent<SpriteRenderer>().color = Color.red;
            StartCoroutine(TimeOfInvulnerability());
            
            HpActual -= damage <= defense ? 0 : damage - defense;
            
            GameManager.Instance.SetHealtBar(HpActual, playerNumber);
            if (HpActual < 0)
                ChangeState(SwitchMachineStates.DIE);
        }
    }
    
    public void GoldReward(int gold)
    {
        this.gold += gold;
        GameManager.Instance.SetGoldCanvas(this.gold, playerNumber);
    }

    public void ExperienceReward(float experience)
    {
        this.experience += experience;
        if (this.experience > experienceRequired)
        {
            this.experience -= experienceRequired;
            playerLevel++;
            experienceRequired += experienceRequired * 2;
            GameManager.Instance.SetExprienceMaxBar(experienceRequired, playerNumber);
            LevelUp();
        }
        GameManager.Instance.SetExprienceBar(this.experience, playerNumber);
    }

    public void LevelUp()
    {
        HpMax += 2;
        HpActual += 1;
        speed += 0.1f;
        damage += .2f;
        if (playerLevel % 3 == 0)
        {
            defense += 1;
        }
        GameManager.Instance.SetPlayerLevelCanvas(playerLevel, playerNumber);
        GameManager.Instance.SetMaxHealtBar(HpMax, playerNumber);
        GameManager.Instance.SetHealtBar(HpActual, playerNumber);
    }

    #endregion

    //*********************** INVENTORY *********************
    
    #region Inventory

    public void OpenInventory(InputAction.CallbackContext callback)
    {
        if (callback.performed)
        {
            if(playerNumber==1)
                GameManager.Instance.ShowOrHideInventoryPlayer1();
            else
                GameManager.Instance.ShowOrHideInventoryPlayer2();
        
            if (!GameManager.Instance.GetGameIsPaused())
                GameManager.Instance.PauseGame(false);
            else
                GameManager.Instance.ResumeGame();
        }
    }

    public void OpenShop(InputAction.CallbackContext callback)
    {
        
        GameManager.Instance.IntoShop(playerNumber);
    }

     public bool HealHp(int heal)
    {
        if (HpActual == HpMax)
            return false;
        
        HpActual += heal;
        if (HpActual > HpMax)
            HpActual = HpMax;
        
        GameManager.Instance.audioSFXManager.HealAudioPlay();
        GameManager.Instance.SetHealtBar(HpActual, playerNumber);
        return true;
    }

    public bool LooseHp(int dmg)
    {
        HpActual -= dmg;
        if (HpActual <= 0)
        {
            ChangeState(SwitchMachineStates.DIE);
            return false;
        }
        GameManager.Instance.audioSFXManager.NegativeHealAudioPlay();
        GameManager.Instance.SetHealtBar(HpActual, playerNumber);

        return true;
    }

    public void UseItemSlot1(InputAction.CallbackContext callback)
    {
        if (callback.performed)
            GameManager.Instance.UseItemFromBackpack(playerNumber, 1);
    }

    public void UseItemSlot2(InputAction.CallbackContext callback)
    {
       
        if (callback.performed)
            GameManager.Instance.UseItemFromBackpack(playerNumber, 2);
    }

    public void UpdateArmor()
    {
        GetComponent<SpriteRenderer>().sprite = armor.getSprite();
        GameManager.Instance.SetPlayer(this);
    }

    public void UpdateWeapon()
    {
        playerHitbox.GetComponent<SpriteRenderer>().sprite = weapon.getSprite();
        GameManager.Instance.SetPlayer(this);
    }

    public void SetAttack(float attack)
    {
        damage = attack;
        if (damage <= 0)
            damage = 1;
    }

    public float GetAttack()
    {
        return damage;
    }

    public void SetDefense(float defensa)
    {
        defense = defensa;
    }

    public float GetDefense()
    {
        return defense;
    }

    public ArmorObject Armor
    {
        get => armor;
        set => armor = value;
    }

    public WeaponObject Weapon
    {
        get => weapon;
        set => weapon = value;
    }


    #endregion
   
    // ******************** FINAL *****************
    
    public void SaveCurrentPlayer()
    {
        currentPlayerSo.armorPlayer = armor;
        currentPlayerSo.weaponPlayer = weapon;
        currentPlayerSo.experienceRequired = experienceRequired;
        currentPlayerSo.playerNumber = playerNumber;
        currentPlayerSo.playerLevel = playerLevel;
        currentPlayerSo.experience = experience;
        currentPlayerSo.gold = gold;
        currentPlayerSo.maxHp = HpMax;
        currentPlayerSo.hp = HpActual;
        currentPlayerSo.speed = speed;
        currentPlayerSo.attack = damage;
        currentPlayerSo.defense = defense;
        currentPlayerSo.round = GameManager.Instance.Round;
    }

    private void LoadCurrentPlayer()
    {
        armor = currentPlayerSo.armorPlayer;
        weapon = currentPlayerSo.weaponPlayer;
        playerLevel = currentPlayerSo.playerLevel;
        playerNumber = currentPlayerSo.playerNumber;
        experienceRequired = currentPlayerSo.experienceRequired;
        experience = currentPlayerSo.experience;
        gold = currentPlayerSo.gold;
        HpMax = currentPlayerSo.maxHp;
        HpActual = currentPlayerSo.hp;
        speed = currentPlayerSo.speed;
        damage = currentPlayerSo.attack;
        defense = currentPlayerSo.defense;
        ricochetActive = playerSo.abilityRicochet;
        explosionActive = playerSo.abilityExplosion;
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("LastRoomTag"))
        {
            GameManager.Instance.SaveCurrentsPlayersInGM();
            GameManager.Instance.LoadBossScene();
        }

        if (other.CompareTag("DungeonGate"))
        {
            GameManager.Instance.SaveCurrentsPlayersInGM();
            GameManager.Instance.LoadDungeonScene();
        }
    }

    private void OnDestroy()
    {
        GameManager.Instance.DestroyPlayer(this);
    }

    
    public SaveData.PlayerData Save()
    {
        SaveCurrentPlayer();
        return new SaveData.PlayerData(currentPlayerSo);
    }

    public void Load(SaveData.PlayerData playerData)
    {
        armor = playerData.armorPlayer;
        weapon = playerData.weaponPlayer;
        playerNumber = playerData.playerNumber;
        experienceRequired = playerData.experienceRequired;
        experience = playerData.experience;
        gold = playerData.gold;
        HpMax = playerData.HpMax;
        HpActual = playerData.HpActual;
        speed = playerData.speed;
        damage = playerData.damage;
        defense = playerData.defense;
        playerLevel = playerData.playerLevel;
        
        ricochetActive = playerData.ricochetActive;
        explosionActive = playerData.explosionActive;
        
        SaveCurrentPlayer();
        GameManager.Instance.LoadArmorAndWeaponOnBackpack();
        UpdateAllUI();
        
    }
    
}