using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class BackpackController : MonoBehaviour
{
    [SerializeField] private Backpack backpack;
    [SerializeField] private GameObject optionsMenu;
    
    [SerializeField] private GameObject askPosition;
    [SerializeField] [Range(1, 2)] private int numPlayer;
    [SerializeField] private GameObject fastAcces1;
    [SerializeField] private GameObject fastAcces2;
    [SerializeField] private WeaponObject weaponNone;
    [SerializeField] private WeaponObject weapon2None;
    [SerializeField] private ArmorObject armor1None;
    [SerializeField] private ArmorObject armor2None;
    
    private GameObject player;
    private ObjectClass itemClicked;

    private void Awake()
    {
        backpack.DeEquipAllItems();
    }

    public void EquipItem()
    {
        if (itemClicked is IEquipable)
        {
            player = GameManager.Instance.GetPlayer(numPlayer).gameObject;
            IEquipable equipableItem = itemClicked as IEquipable;
            if (equipableItem.getEquiped())
            {
                if (equipableItem is WeaponObject)
                {
                    if (numPlayer == 1)
                    {
                        equipableItem.Unequip(player);
                        weaponNone.Equip(player);
                        player.GetComponent<PlayerController>().UpdateWeapon();
                        GameManager.Instance.SetInfoMensaje("You unnequiped "+itemClicked.name);
                    }
                    if (numPlayer == 2)
                    {
                        equipableItem.Unequip(player);
                        weapon2None.Equip(player);
                        player.GetComponent<PlayerController>().UpdateWeapon();
                        GameManager.Instance.SetInfoMensaje("You unnequiped "+itemClicked.name);
                    }
                }
                else
                {
                    if (numPlayer == 1)
                    {
                        equipableItem.Unequip(player);
                        armor1None.Equip(player);
                        player.GetComponent<PlayerController>().UpdateArmor();
                        GameManager.Instance.SetInfoMensaje("You unnequiped "+itemClicked.name);
                    }
                    if (numPlayer == 2)
                    {
                        equipableItem.Unequip(player);
                        armor2None.Equip(player);
                        player.GetComponent<PlayerController>().UpdateArmor();
                        GameManager.Instance.SetInfoMensaje("You unnequiped "+itemClicked.name);
                    }
                    
                }

                updateUI();
            }
            else
            {
                equipableItem.Equip(player);
                if (equipableItem is WeaponObject)
                {
                    player.GetComponent<PlayerController>().UpdateWeapon();
                    GameManager.Instance.SetInfoMensaje("You equiped "+itemClicked.name);
                }
                else
                {
                    player.GetComponent<PlayerController>().UpdateArmor();
                    GameManager.Instance.SetInfoMensaje("You equiped "+itemClicked.name);
                }

                updateUI();
            }

            optionsMenu.SetActive(false);
        }
        else if (itemClicked is IConsumible)
            askPosition.SetActive(true);

        optionsMenu.SetActive(false);
    }

    public void FastEquip(int position)
    {
        if (position == 0)
        {
            if (itemClicked.fastAcces)
                GameManager.Instance.SetInfoMensaje("You already have this item equipped");
            else
            {
                fastAcces1.gameObject.SetActive(true);
                fastAcces1.GetComponent<InventoryItem>().Load(backpack.GetItem(itemClicked));
                fastAcces1.GetComponent<InventoryItem>().setItemSlot(backpack.GetItem(itemClicked));
                itemClicked.fastAcces = true;
            }

            askPosition.SetActive(false);
        }
        else
        {
            if (itemClicked.fastAcces)
                GameManager.Instance.SetInfoMensaje("You already have this item equipped");
            else
            {
                fastAcces2.gameObject.SetActive(true);
                fastAcces2.GetComponent<InventoryItem>().Load(backpack.GetItem(itemClicked));
                fastAcces2.GetComponent<InventoryItem>().setItemSlot(backpack.GetItem(itemClicked));
                itemClicked.fastAcces = true;
            }

            askPosition.SetActive(false);
        }

        askPosition.SetActive(false);
    }


    public void UseItem(int slotused)
    {
        ObjectClass oc;
        if (slotused == 1)
        {
            oc = fastAcces1.GetComponent<InventoryItem>().getItemSlot().item;
        }
        else
        {
            oc = fastAcces2.GetComponent<InventoryItem>().getItemSlot().item;
        }

        player = GameManager.Instance.GetPlayer(numPlayer).gameObject;
        IConsumible equipableItem = oc as IConsumible;
        equipableItem.UsedBy(player);
        backpack.RemoveItem(oc);
        updateUI();
    }

    public void ClickOnItem(ObjectClass item)
    {
        GameManager.Instance.ActiveOrDisableItemOptions(numPlayer);
        itemClicked = item;
        
    }

    //Funcion para lanzar un item
    public void ThrowItem()
    {
        backpack.RemoveItem(itemClicked);
        GameManager.Instance.SetInfoMensaje("You dropped "+itemClicked.getName());
        optionsMenu.SetActive(false);
        updateUI();
        itemClicked = null;
    }

    //Funcion que se llamara la obtener un item
    public bool AddItem(ObjectClass item)
    {
        if (item is IStackable)
        {
            IStackable istack = item as IStackable;
           if (!backpack.ContainsItem(item))
           {
                   backpack.AddItem(item);
                   updateUI();
                   return true;
           }
            
            if (backpack.getSlot(item).amount < istack.GetMaxStack())
            {
                backpack.AddItemStackeable(item);
                updateUI();
                return true;
            }
            
            
            updateUI();
            return false;
        }

        backpack.AddItem(item);
        updateUI();
        return true;
    }

    public void CancelMenu()
    {
        optionsMenu.SetActive(false);
        itemClicked = null;
    }

    public void updateUI()
    {
        InventoryItem a1 = fastAcces1.GetComponent<InventoryItem>();
        InventoryItem a2 = fastAcces2.GetComponent<InventoryItem>();
        if (fastAcces1.active)
        {
            if (a1.getItemSlot().amount == 0)
            {
                fastAcces1.SetActive(false);
            }
            else
            {
                a1.Load(a1.getItemSlot());
            }
        }

        if (fastAcces2.active)
        {
            if (a2.getItemSlot().amount == 0)
                fastAcces2.SetActive(false);
            else
                a2.Load(a2.getItemSlot());
        }
        GameManager.Instance.RefreshBackpackUI(numPlayer);
    }

    public void DisableOptionsMenu()
    {
        optionsMenu.SetActive(false);
    }
}