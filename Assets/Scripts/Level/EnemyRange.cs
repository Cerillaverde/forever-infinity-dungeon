using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRange : EnemyControl
{
    [SerializeField] private SOEnemy statsEnemy;
    private GameObject poolShoot;
    private bool waitingForNextPath;
    Vector2 nextDestination;

    private bool inAttack = false;

    void Start()
    {
        SetupScriptableObject();

        poolShoot = LevelManager.Instance.GetPoolShoot();

        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.updateRotation = false;
        agent.updateUpAxis = false;

        waitingForNextPath = false;

        originPosition = transform.position;
        InitState(SwitchMachineStates.IDLE);
    }

    private void SetupScriptableObject()
    {
        int level = GameManager.Instance.Round;
        experienceMin = statsEnemy.experienceMin * level;
        experienceMax = statsEnemy.experienceMax * level;
        goldMax = statsEnemy.goldMax + (int)(statsEnemy.goldMax * level / 5);
        goldMin = statsEnemy.goldMin + (int)(statsEnemy.goldMin * level / 5);
        hpMax = statsEnemy.maxHP + level * 2;
        hpActual = hpMax;
        moveSpeed = statsEnemy.moveSpeed + level / 20f;
        damage = statsEnemy.damage + level;
        rangeDetection = statsEnemy.rangeDetection;
        rangeAttack = statsEnemy.rangeAttack;
        colorOfEnemy = GetComponent<SpriteRenderer>().color;
        colorOfEnemy = statsEnemy.color;
    }

    void Update()
    {
        playerCollider = Physics2D.OverlapCircle(transform.position, rangeDetection, layerMask);
        UpdateState();

        if (!inmune && playerCollider != null)
            TrackedPlayer(playerCollider);

        float distance = Vector2.Distance(agent.destination, transform.position);
        if (playerCollider == null && playerTraked)
            StartCoroutine(StayOnSite());
        else if (distance < 1.2f && !waitingForNextPath && playerCollider == null)
            StartCoroutine(CooldownToNextPoint(5f));
    }

    IEnumerator StayOnSite()
    {
        agent.speed = 0;
        waitingForNextPath = true;
        yield return new WaitForSeconds(1.5f);
        agent.SetDestination(originPosition);
    }

    void TrackedPlayer(Collider2D playerCol)
    {
        StopCoroutine(StayOnSite());
        agent.SetDestination(playerCol.gameObject.transform.position);
        float distance = Vector2.Distance(agent.destination, this.transform.position);
        if (distance < rangeAttack)
        {
            agent.speed = 0;
            if (canAttack)
            {
                ShootToPlayer();
                ChangeState(SwitchMachineStates.ATTACK);
            }
        }
        else
            agent.speed = moveSpeed;
    }

    private void ShootToPlayer()
    {
        canAttack = false;
        for (int i = 0; i < poolShoot.transform.childCount; i++)
        {
            Transform tActual = poolShoot.transform.GetChild(i);
            if (!tActual.gameObject.activeSelf)
            {
                tActual.gameObject.SetActive(true);
                tActual.transform.position = transform.position;
                tActual.gameObject.GetComponent<Rigidbody2D>().velocity =
                    (agent.destination - tActual.transform.position).normalized * 8;
                tActual.gameObject.GetComponent<BulletControl>().SetBulletDamage(damage);
                break;
            }
        }

        StartCoroutine(ShootCoolDown(3));
    }

    IEnumerator ShootCoolDown(float time)
    {
        yield return new WaitForSeconds(time);
        canAttack = true;
    }

    IEnumerator CooldownToNextPoint(float time)
    {
        agent.speed = 0;
        waitingForNextPath = true;
        yield return new WaitForSeconds(time);
        GotoNextPoint();
    }


    void GotoNextPoint()
    {
        float random = Random.Range(2, 5);
        switch (Random.Range(0, 4))
        {
            case 0:
                nextDestination = new Vector2(transform.position.x + random, transform.position.y);
                agent.SetDestination(nextDestination);
                break;
            case 1:
                nextDestination = new Vector2(transform.position.x - random, transform.position.y);
                agent.SetDestination(nextDestination);
                break;
            case 2:
                nextDestination = new Vector2(transform.position.x + 0.01f, (transform.position.y + random));
                agent.SetDestination(nextDestination);
                break;
            case 3:
                nextDestination = new Vector2(transform.position.x + 0.01f, (transform.position.y - random));
                agent.SetDestination(nextDestination);
                break;
        }

        agent.speed = moveSpeed;
        waitingForNextPath = false;
    }


    public override void OnHit(float damage)
    {
        hpActual -= damage;
        if (hpActual > 0)
            ChangeState(SwitchMachineStates.RECIVEHIT);
        else
            ChangeState(SwitchMachineStates.DIE);
    }

    // ********************** MACHINE STATS ********************

    #region SwitchMachineStates

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == currentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        this.currentState = currentState;
        switch (this.currentState)
        {
            case SwitchMachineStates.IDLE:

                agent.speed = 0;
                colorOfEnemy = new Color(colorOfEnemy.r, colorOfEnemy.g, colorOfEnemy.b, 120);

                break;


            case SwitchMachineStates.WALK:
                colorOfEnemy = statsEnemy.color;
                break;


            case SwitchMachineStates.ATTACK:
                agent.speed = 0;
                inAttack = true;
                break;

            case SwitchMachineStates.RECIVEHIT:
                agent.speed = 0;
                GetComponent<SpriteRenderer>().color = Color.red;
                StartCoroutine(StunTime());
                break;

            case SwitchMachineStates.DIE:
                Destroy(gameObject);
                break;

            default:
                break;
        }
    }

    IEnumerator StunTime()
    {
        yield return new WaitForSeconds(1f);
        ChangeState(lastState);
    }

    private void ExitState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.ATTACK:
                inAttack = false;
                break;

            case SwitchMachineStates.RECIVEHIT:
                inmune = false;
                GetComponent<SpriteRenderer>().color = statsEnemy.color;
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                if (agent.speed != 0 && !inAttack)
                    ChangeState(SwitchMachineStates.WALK);

                lastState = currentState;
                break;

            case SwitchMachineStates.WALK:

                if (agent.speed == 0 && !inAttack)
                    ChangeState(SwitchMachineStates.IDLE);

                lastState = currentState;
                break;

            case SwitchMachineStates.ATTACK:
                break;
            case SwitchMachineStates.RECIVEHIT:
                break;
            case SwitchMachineStates.DIE:
                break;

            default:
                break;
        }
    }

    #endregion

    private void OnDestroy()
    {
        GameManager.Instance.GiveExperience(experienceMin, experienceMax);
        GameManager.Instance.GiveGold(goldMax, goldMin);
    }
}