using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMele : EnemyControl
{
    [SerializeField] private SOEnemy statsEnemy;
    private float timeOfAtackActive = 0.8f;
    private bool inAttack = false;
    public GameEvent OnBossDied;

    void Start()
    {
        // statsEnemy = LevelManager.Instance.GetRNGNormalEnemy();

        SetupScriptableObject();

        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.autoBraking = false;
        agent.updateRotation = false;
        agent.updateUpAxis = false;

        enemyHitbox.gameObject.SetActive(false);

        originPosition = transform.position;
        InitState(SwitchMachineStates.IDLE);
    }

    private void SetupScriptableObject()
    {
        int level = GameManager.Instance.Round;
        experienceMin = statsEnemy.experienceMin * level;
        experienceMax = statsEnemy.experienceMax * level;
        goldMax = statsEnemy.goldMax + (int)(statsEnemy.goldMax * level / 5);
        goldMin = statsEnemy.goldMin + (int)(statsEnemy.goldMin * level / 5);
        hpMax = statsEnemy.maxHP + level * 2;
        hpActual = hpMax;
        moveSpeed = statsEnemy.moveSpeed + level / 20f;
        damage = statsEnemy.damage + level;
        rangeDetection = statsEnemy.rangeDetection;
        rangeAttack = statsEnemy.rangeAttack;
        colorOfEnemy = GetComponent<SpriteRenderer>().color;
        colorOfEnemy = statsEnemy.color;
    }

    void Update()
    {
        playerCollider = Physics2D.OverlapCircle(transform.position, rangeDetection, layerMask);
        UpdateState();

        if (!inmune && playerCollider != null)
            TrackedPlayer(playerCollider);

        float distance = Vector2.Distance(originPosition, this.transform.position);
        if (playerCollider == null && playerTraked)
            StartCoroutine(StayOnSite());
        else if (distance > 0.4f && playerCollider == null)
            agent.SetDestination(originPosition);
        else if (distance < 0.4f && playerCollider == null)
            agent.speed = 0;
    }

    IEnumerator StayOnSite()
    {
        agent.speed = 0;
        yield return new WaitForSeconds(1.5f);
        agent.speed = moveSpeed;
        playerTraked = false;
    }

    void TrackedPlayer(Collider2D playerCol)
    {
        GameObject player = playerCol.gameObject;
        float distance = Vector2.Distance(player.transform.position, transform.position);
        if (distance < rangeAttack)
        {
            agent.speed = 0;
            if (canAttack)
            {
                ShootToPlayer();
                ChangeState(SwitchMachineStates.ATTACK);
            }
        }
        else
        {
            agent.speed = moveSpeed;
            agent.SetDestination(player.transform.position);
        }

        playerTraked = true;
    }

    private void ShootToPlayer()
    {
        canAttack = false;
        enemyHitbox.GetComponent<HitboxInfo>().SetDamage(damage);
        enemyHitbox.gameObject.SetActive(true);
        StartCoroutine(EndAtack());
        StartCoroutine(ShootCoolDown(2f));
    }

    IEnumerator ShootCoolDown(float time)
    {
        yield return new WaitForSeconds(time);
        canAttack = true;
    }

    public override void OnHit(float damage)
    {
        hpActual -= damage;
        if (hpActual > 0)
            ChangeState(SwitchMachineStates.RECIVEHIT);
        else
            ChangeState(SwitchMachineStates.DIE);
    }

    // ********************** MACHINE STATS ********************

    #region SwitchMachineStates

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == currentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        this.currentState = currentState;
        switch (this.currentState)
        {
            case SwitchMachineStates.IDLE:

                agent.speed = 0;
                colorOfEnemy = new Color(colorOfEnemy.r, colorOfEnemy.g, colorOfEnemy.b, 120);

                break;


            case SwitchMachineStates.WALK:
                colorOfEnemy = statsEnemy.color;
                break;


            case SwitchMachineStates.ATTACK:
                agent.speed = 0;
                inAttack = true;
                break;

            case SwitchMachineStates.RECIVEHIT:
                agent.speed = 0;
                GetComponent<SpriteRenderer>().color = Color.red;
                StartCoroutine(StunTime());
                break;

            case SwitchMachineStates.DIE:
                Destroy(gameObject);
                break;

            default:
                break;
        }
    }

    IEnumerator StunTime()
    {
        yield return new WaitForSeconds(1f);
        ChangeState(lastState);
    }

    private void ExitState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.ATTACK:
                inAttack = false;
                break;

            case SwitchMachineStates.RECIVEHIT:
                inmune = false;
                GetComponent<SpriteRenderer>().color = statsEnemy.color;
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                if (agent.speed != 0 && !inAttack)
                    ChangeState(SwitchMachineStates.WALK);

                lastState = currentState;
                break;

            case SwitchMachineStates.WALK:

                if (agent.speed == 0 && !inAttack)
                    ChangeState(SwitchMachineStates.IDLE);

                lastState = currentState;
                break;

            case SwitchMachineStates.ATTACK:
                break;
            case SwitchMachineStates.RECIVEHIT:
                break;
            case SwitchMachineStates.DIE:
                break;

            default:
                break;
        }
    }

    IEnumerator EndAtack()
    {
        yield return new WaitForSeconds(timeOfAtackActive);
        enemyHitbox.gameObject.SetActive(false);
        ChangeState(lastState);
    }

    #endregion

    private void OnDestroy()
    {
        if (gameObject.CompareTag("Boss"))
            OnBossDied.Raise();
        // GameManager.Instance.BossDie();
        GameManager.Instance.GiveExperience(experienceMin, experienceMax);
        GameManager.Instance.GiveGold(goldMax, goldMin);
    }
}