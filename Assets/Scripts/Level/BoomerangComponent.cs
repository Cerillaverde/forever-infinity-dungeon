using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoomerangComponent : MonoBehaviour
{

    [SerializeField] private SOAbility soAbility;
    private float speed;
    private float damageAdaptable;
    private float timeOfCooldown;
    private float distanceToReturn;
    
    private PlayerController player;
    private Vector3 origin;
    private bool wallCollide = false;
    private bool isComeback = false;

    private void Awake()
    {
        speed = soAbility.abilityAdvanceSpeed;
        damageAdaptable = soAbility.damageAdaptable;
        timeOfCooldown = soAbility.timeOfCooldown;
        distanceToReturn = soAbility.scopeAbility;
    }

    private void Update()
    {
        float distance = Vector3.Distance(origin, transform.position);
        if (isComeback)
            GetComponent<Rigidbody2D>().velocity = (Vector2)((player.gameObject.transform.position - transform.position).normalized * speed);
        if (distance > distanceToReturn || wallCollide)
            BoomerangComeBack();
    }

    public void ShootWhereVisualize(PlayerController nPlayer)
    {
        player = nPlayer;
        origin = transform.position;
        GetComponent<HitboxInfo>().SetDamage(player.GetDamage() * (damageAdaptable / 100 + 1));
        Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        GetComponent<Rigidbody2D>().velocity = (Vector2)((position - transform.position).normalized * speed);
        player.StartCooldownAbility1(timeOfCooldown);
        StartCoroutine(RotateBoomerang());
    }

    IEnumerator RotateBoomerang()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.01f);
            transform.Rotate(0, 0, 5f);
        }
    }

    private void BoomerangComeBack()
    {
        isComeback = true;
    }

    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && isComeback)
        {
            Destroy(gameObject);
        }

        if (other.gameObject.layer == 0 && !other.CompareTag("Player"))
        {
            wallCollide = true;
            BoomerangComeBack();
        }
    }
}