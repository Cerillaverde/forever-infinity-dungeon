using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;

public class CameraFollowTarget : MonoBehaviour
{
    [SerializeField] private Transform followTarget;

    [Min(0f)] [SerializeField] private float _Delay = 1f;
    /*

    [SerializeField] private bool m_LERP = true;
    private Vector3 m_TargetPosition;
    private Vector3 m_BasePosition;
    [SerializeField] private float m_LerpDelay = .5f;
    [SerializeField] private float m_CurrentLerpDelay;
    */

    public Transform Target
    {
        get => followTarget;
        set
        {
            followTarget = value;
            transform.position = followTarget.transform.position + Vector3.forward * -10;
        }
    }

    private void LateUpdate()
    {
        if (followTarget == null) return;
        transform.position = Vector3.Lerp(followTarget.position + Vector3.forward * -10, transform.position,
            _Delay * Time.deltaTime);
        /*
        if (m_LERP)
        {
            //transform.position = Vector3.Lerp(m_FollowTarget.position + Vector3.forward * -10, transform.position, Time.deltaTime);
            Vector3 targetPosition = m_FollowTarget.position + Vector3.forward * -10;
            if (m_TargetPosition != targetPosition)
            {
                m_TargetPosition = targetPosition;
                m_BasePosition = transform.position;
                m_CurrentLerpDelay = 0f;
            }

            m_CurrentLerpDelay = Mathf.Clamp(m_CurrentLerpDelay + Time.deltaTime, 0f, m_LerpDelay);
            transform.position = Vector3.Lerp(m_BasePosition, targetPosition, m_CurrentLerpDelay);
        }
        else
            transform.position = m_FollowTarget.position + Vector3.forward * -10;
        */
    }
}