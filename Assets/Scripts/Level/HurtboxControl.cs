using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtboxControl : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (transform.parent.TryGetComponent(out PlayerController player))
            player.OnHit(collision.gameObject.GetComponent<HitboxInfo>().GetDamage());

        if (transform.parent.TryGetComponent(out EnemyControl enemy) && collision.gameObject.GetComponent<HitboxInfo>() != null)
            enemy.OnHit(collision.gameObject.GetComponent<HitboxInfo>().GetDamage());
        
    }
}