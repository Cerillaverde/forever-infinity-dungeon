using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RepelComponent : MonoBehaviour
{
    [SerializeField] private SOAbility soAbility;
    private float timeOfCooldown;
    private float scopeOfAbility;
    private float speed;
    
    private PlayerController player;

    private void Awake()
    {
        timeOfCooldown = soAbility.timeOfCooldown;
        scopeOfAbility = soAbility.scopeAbility;
        speed = soAbility.abilityAdvanceSpeed;
    }

    public void RepelEnemies(PlayerController nPlayer)
    {
        player = nPlayer;
        transform.position = nPlayer.gameObject.transform.position;
        transform.localScale = new Vector3(1, 1, 1);
        player.StartCooldownAbility2(timeOfCooldown);
        StartCoroutine(IncrementRadius());
    }

    IEnumerator IncrementRadius()
    {
        while (transform.localScale.x <= scopeOfAbility)
        {
            yield return new WaitForSeconds(speed);
            transform.localScale += new Vector3(0.2f, 0.2f, 0);
        }

        player.EndAbility2();
        Destroy(gameObject);
    }

}
