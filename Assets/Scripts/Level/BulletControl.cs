using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletControl : MonoBehaviour
{
    private float hitDamage;
    public void SetBulletDamage(float damage) { hitDamage = damage; }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GetComponent<HitboxInfo>().SetDamage(hitDamage);
        gameObject.SetActive(false);
    }
}
