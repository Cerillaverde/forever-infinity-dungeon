using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionBoomerangPreview : MonoBehaviour
{
    private PlayerController player;
    private bool waitForPlayer = false;

    void Update()
    {
        if (waitForPlayer)
        {
            transform.position = player.gameObject.transform.position;
            Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 rotation = position - transform.position;
            float rot = Mathf.Atan2(rotation.y, rotation.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0, 0, rot - 90);
        }
    }

    public void VisualizeToShoot(PlayerController nPlayer)
    {
        player = nPlayer;
        waitForPlayer = true;
    }

    public void PreviewDestroyHimself()
    {
        Destroy(this.gameObject);
    }
}