using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class CorridorFirstDungeonGenerator : SimpleRandomWalkDungeon
{
    [SerializeField] private int corridorLenght = 14;
    [SerializeField] private int corridorCount = 5;

    [SerializeField] [Range(0.1f, 1)] private float roomPercent = 0.8f;

    protected override void RunProceduralGeneration()
    {
        CorridorFirstGeneration();
    }

    private void CorridorFirstGeneration()
    {
        HashSet<Vector2Int> floorPos = new HashSet<Vector2Int>();
        HashSet<Vector2Int> potentialRoomsPos = new HashSet<Vector2Int>();

        List<List<Vector2Int>> corridors = CreateCorridors(floorPos, potentialRoomsPos);

        HashSet<Vector2Int> roomPos = CreateRooms(potentialRoomsPos);

        List<Vector2Int> deadEnds = FindAllDeadEnds(floorPos);

        CreateRoomsDeadEnd(deadEnds, roomPos);

        floorPos.UnionWith(roomPos);

        for (int i = 0; i < corridors.Count; i++)
        {
            // corridors[i] = IncreaseCorridorsSizeByOne(corridors[i]);
            corridors[i] = IncreaseCorridorBrush3by3(corridors[i]);
            floorPos.UnionWith(corridors[i]);
        }

        visualizeTilemap.PaintFloorTiles(floorPos);
        WallDungeonGenerator.CreateDungeonWalls(floorPos, visualizeTilemap);
    }

    private List<Vector2Int> IncreaseCorridorBrush3by3(List<Vector2Int> corridor)
    {
        List<Vector2Int> newCorridor = new List<Vector2Int>();

        for (int i = 1; i < corridor.Count; i++)
        {
            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    newCorridor.Add(corridor[i - 1] + new Vector2Int(x, y));
                }
            }
        }

        return newCorridor;
    }

    private List<Vector2Int> IncreaseCorridorsSizeByOne(List<Vector2Int> corridor)
    {
        List<Vector2Int> newCorridor = new List<Vector2Int>();

        Vector2Int previousDirection = Vector2Int.zero;

        for (int i = 1; i < corridor.Count; i++)
        {
            Vector2Int directionFromCell = corridor[i] - corridor[i - 1];

            if (previousDirection != Vector2Int.zero && directionFromCell != previousDirection)
            {
                for (int x = -1; x < 2; x++)
                {
                    for (int y = -1; y < 2; y++)
                    {
                        newCorridor.Add(corridor[i - 1] + new Vector2Int(x, y));
                    }
                }

                previousDirection = directionFromCell;
            }
            else
            {
                previousDirection = directionFromCell;

                Vector2Int newCorridorTileOffset = GetDirection90From(directionFromCell);

                newCorridor.Add(corridor[i - 1]);
                newCorridor.Add(corridor[i - 1] + newCorridorTileOffset);
            }
        }

        return newCorridor;
    }

    private Vector2Int GetDirection90From(Vector2Int direction)
    {
        if (direction == Vector2Int.up)
        {
            return Vector2Int.right;
        }

        if (direction == Vector2Int.right)
        {
            return Vector2Int.down;
        }

        if (direction == Vector2Int.down)
        {
            return Vector2Int.left;
        }

        if (direction == Vector2Int.left)
        {
            return Vector2Int.up;
        }

        return Vector2Int.zero;
    }

    private void CreateRoomsDeadEnd(List<Vector2Int> deadEnds, HashSet<Vector2Int> roomFloors)
    {
        foreach (Vector2Int pos in deadEnds)
        {
            if (!roomFloors.Contains(pos))
            {
                HashSet<Vector2Int> room = RunRandomWakl(randomWalkSo, pos);
                roomFloors.UnionWith(room);
            }
        }
    }

    private List<Vector2Int> FindAllDeadEnds(HashSet<Vector2Int> floorPos)
    {
        List<Vector2Int> deadEnds = new List<Vector2Int>();

        foreach (Vector2Int pos in floorPos)
        {
            int neighboursCount = 0;

            foreach (Vector2Int direction in Direction2D.cardinalDirectList)
            {
                if (floorPos.Contains(pos + direction))
                {
                    neighboursCount++;
                }
            }

            if (neighboursCount == 1)
            {
                deadEnds.Add(pos);
            }
        }

        return deadEnds;
    }

    private HashSet<Vector2Int> CreateRooms(HashSet<Vector2Int> potentialRoomsPos)
    {
        HashSet<Vector2Int> roomPos = new HashSet<Vector2Int>();

        int roomToCreateCount = Mathf.RoundToInt(potentialRoomsPos.Count * roomPercent);

        List<Vector2Int> roomToCreate = potentialRoomsPos.OrderBy(x => Guid.NewGuid()).Take(roomToCreateCount).ToList();

        foreach (Vector2Int roomPosi in roomToCreate)
        {
            HashSet<Vector2Int> roomFloor = RunRandomWakl(randomWalkSo, roomPosi);
            roomPos.UnionWith(roomFloor);
        }

        return roomPos;
    }

    private List<List<Vector2Int>> CreateCorridors(HashSet<Vector2Int> floorPos, HashSet<Vector2Int> potentialRoomsPos)
    {
        Vector2Int currentPos = startPos;

        potentialRoomsPos.Add(currentPos);

        List<List<Vector2Int>> corridors = new List<List<Vector2Int>>();

        for (int i = 0; i < corridorCount; i++)
        {
            List<Vector2Int> corridor = ProceduralAlgorithms.RandomWalkCorridor(currentPos, corridorLenght);
            corridors.Add(corridor);
            currentPos = corridor[corridor.Count - 1];
            potentialRoomsPos.Add(currentPos);
            floorPos.UnionWith(corridor);
        }

        return corridors;
    }
}