using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatePlacer : MonoBehaviour
{
    [SerializeField] private GameObject gatePrefab;

    private GameObject gate;

    private DungeonData dungeonData;

    private void Awake()
    {
        dungeonData = FindObjectOfType<DungeonData>();
        if (dungeonData == null)
        {
            dungeonData = gameObject.AddComponent<DungeonData>();
        }
    }

    public void PlaceGate()
    {
        gate = Instantiate(gatePrefab);
        gate.SetActive(false);
        gate.transform.localPosition =
            new Vector3(dungeonData.startPosRandomWalk.x, dungeonData.startPosRandomWalk.y, 0);
    }

    public void ActiveGate()
    {
        if (gate != null)
            gate.SetActive(true);
    }
}