using System.Collections;
using System.Collections.Generic;
using NavMeshPlus.Components;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using Vector2 = System.Numerics.Vector2;

public class BossPlayerPlacer : MonoBehaviour
{
    [SerializeField] private NavMeshSurface navigation;
    [SerializeField] private GameObject bossPrefab;
    [SerializeField] private GameObject[] playerPrefab;
    [SerializeField] private CameraFollowTarget cameraPlayer1BoosFight;
    [SerializeField] private CameraFollowTarget cameraPlayer2BoosFight;

    // DATA
    private DungeonData dungeonData;

    private void Awake()
    {
        dungeonData = FindObjectOfType<DungeonData>();
        if (dungeonData == null)
        {
            dungeonData = gameObject.AddComponent<DungeonData>();
        }
    }

    public void PlacePlayerAndBoss()
    {
        navigation.BuildNavMeshAsync();

        GameObject boss = Instantiate(bossPrefab);
        GameObject player = Instantiate(playerPrefab[0]);
        GameObject player2 = Instantiate(playerPrefab[1]);

        Light2D lightPlayer = player.GetComponent<Light2D>();
        Light2D lightPlayer2 = player2.GetComponent<Light2D>();

        lightPlayer.pointLightInnerRadius = 4f;
        lightPlayer.pointLightOuterRadius = 6f;

        lightPlayer2.pointLightInnerRadius = 4f;
        lightPlayer2.pointLightOuterRadius = 6f;

        player.transform.localPosition =
            new Vector3(dungeonData.startPosRandomWalk.x + 5, dungeonData.startPosRandomWalk.y, 0);
        player2.transform.localPosition =
            new Vector3(dungeonData.startPosRandomWalk.x - 5, dungeonData.startPosRandomWalk.y, 0);

        cameraPlayer1BoosFight.Target = player.transform;
        cameraPlayer2BoosFight.Target = player2.transform;
        boss.transform.localPosition =
            new Vector3(dungeonData.startPosRandomWalk.x, dungeonData.startPosRandomWalk.y, 0);
    }
}