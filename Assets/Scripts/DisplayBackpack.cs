using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayBackpack : MonoBehaviour
{
    [SerializeField] private GameObject displayBackpackParent;

    [SerializeField] private GameObject displayItemPrefab;

    [SerializeField] private Backpack backpack;

    private void ClearDisplay()
    {
        foreach (Transform child in displayBackpackParent.transform)
            Destroy(child.gameObject);
    }

    private void FillDisplay()
    {
        foreach (Backpack.ItemSlot itemSlot in backpack.ItemSlots)
        {
            GameObject displayedItem = Instantiate(displayItemPrefab, displayBackpackParent.transform);
            displayedItem.GetComponent<InventoryItem>().Load(itemSlot);
        }
    }

    public void RefreshBackpack()
    {
        ClearDisplay();
        FillDisplay();
    }

    public Backpack Backpack
    {
        get => backpack;
        set => backpack = value;
    }
}