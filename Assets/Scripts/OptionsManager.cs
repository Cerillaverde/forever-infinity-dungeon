using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour
{
    [SerializeField] private SOGeneralOptions SOGeneralStats;
    [SerializeField] private Slider BGVolume;
    [SerializeField] private Slider SFXVolume;

    private void Start()
    {
        BGVolume.value = SOGeneralStats.audioBGVolume;
        SFXVolume.value = SOGeneralStats.audioSFXVolume;
    }

    public void SetVolumeBG()
    {
        SOGeneralStats.audioBGVolume = BGVolume.value;
        GameManager.Instance.audioBGManager.SetVolume();
    }

    public void SetVolumeSFX()
    {
        SOGeneralStats.audioSFXVolume = SFXVolume.value;
        // GameManager.Instance.audioSFXManager.SetVolume();
    }
}
