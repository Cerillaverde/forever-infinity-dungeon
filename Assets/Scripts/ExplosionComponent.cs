using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExplosionComponent : MonoBehaviour
{
    [SerializeField] private SOAbility soAbility;
    private float timeOfCooldown;
    private float damageAdaptable;
    private float scopeOfAbility;
    private float speed;

    private PlayerController player;

    private void Awake()
    {
        timeOfCooldown = soAbility.timeOfCooldown;
        damageAdaptable = soAbility.damageAdaptable;
        scopeOfAbility = soAbility.scopeAbility;
        speed = soAbility.abilityAdvanceSpeed;
    }

    public void ExplosionActive(PlayerController nPlayer)
    {
        player = nPlayer;
        GetComponent<HitboxInfo>().SetDamage(player.GetDamage() * (damageAdaptable / 100 + 1));
        transform.position = nPlayer.gameObject.transform.position;
        transform.localScale = new Vector3(0.5f, 0.5f, 1);
        gameObject.SetActive(true);
        StartCoroutine(IncrementRadius());
        player.StartCooldownAbility2(timeOfCooldown);
    }

    IEnumerator IncrementRadius()
    {
        while (transform.localScale.x <= scopeOfAbility)
        {
            yield return new WaitForSeconds(speed);
            transform.localScale += new Vector3(0.5f, 0.5f, 0);
        }

        player.EndAbility2();
        Destroy(gameObject);
    }

}