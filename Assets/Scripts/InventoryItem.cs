using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour
{
    [Header("Functionality")]
    // [SerializeField] private GameEventItem eventClick;
    [Header("Display")]
    [SerializeField]
    private TextMeshProUGUI m_IDText;

    [SerializeField] private TextMeshProUGUI m_AmountText;
    [SerializeField] private Image m_Image;

    private Backpack.ItemSlot fastAccessItemSlot = null;

    private ObjectClass item;

    public void Load(ObjectClass item)
    {
        m_IDText.text = item.getName();
        m_Image.sprite = item.getIcon();
        this.item = item;
    }

    public void Load(Backpack.ItemSlot itemSlot)
    {
        Load(itemSlot.item);
        
        if (itemSlot.item is IStackable)
        {
            m_AmountText.text = itemSlot.amount.ToString();
        }
        else
        {
            if (itemSlot.item is IEquipable)
            {
                IEquipable equip = itemSlot.item as IEquipable;
                if (equip.getEquiped())
                {
                    m_AmountText.text = "Equiped";
                }
                else
                {
                    m_AmountText.text = "Not Equiped";
                }
            }
        }
    }

    public void RaiseEventClick()
    {
        GameManager.Instance.ClickOnItem(item);
    }

    public void setItemSlot(Backpack.ItemSlot islot)
    {
        if (fastAccessItemSlot == null)
        {
            fastAccessItemSlot = islot;
        }
        else
        {
            fastAccessItemSlot.item.fastAcces = false;
            fastAccessItemSlot = islot;
        }
    }

    public Backpack.ItemSlot getItemSlot()
    {
        return fastAccessItemSlot;
    }
}