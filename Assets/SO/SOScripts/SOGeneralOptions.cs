using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SOGeneralOptions", menuName = "Scriptable Objects/SO General Options")]
public class SOGeneralOptions : ScriptableObject
{
    
    public float audioBGVolume;
    public float audioSFXVolume;

}
