using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "SOPlayer", menuName = "Scriptable Objects/SO Player")]
public class SOPlayer : ScriptableObject
{
    [FormerlySerializedAs("amorPlayer")]
    [Header("Atributtes of player")] [Space(2)]
    
    [Space(2)] public ArmorObject armorPlayer;
    [Space(2)] public WeaponObject weaponPlayer;
    
    [Space(2)] public bool abilityRicochet;
    [Space(2)] public bool abilityBoomerang;
    [Space(2)] public bool abilityExplosion;
    [Space(2)] public bool abilityRepel;
    
    [Space(2)] public float experienceRequired;
    [Space(2)] public int playerLevel;
    [HideInInspector] public float experience;
    [Space(2)] public int gold;

    [Space(2)] public int playerNumber;
    [Space(2)] public float speed;
    [Space(2)] public float maxHp;
    [HideInInspector] public float hp;
    [Space(2)] public float attack;
    [Space(2)] public float defense;
    [HideInInspector] public int round;
}
