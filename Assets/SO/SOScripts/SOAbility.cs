using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "SOAbility", menuName = "Scriptable Objects/SO Abilities")]
public class SOAbility : ScriptableObject
{
    [Header("Atributtes of abilities")] 
    
    [Tooltip("Speed at which the skill advances.")] [Space(2)]
    public float abilityAdvanceSpeed;

    [Tooltip("Multiplies the player's base damage by %.")] [Space(2)]
    public float damageAdaptable;
    
    [Tooltip("Determines the time to be able to use the ability again.")] [Space(2)]
    public float timeOfCooldown;

    [Range(3, 20)] [Tooltip("Determines the scope of the ability.")] [Space(2)]
    public float scopeAbility;
    
    [Space(4)][Header("ONLY FOR RICOCHET ABILITY")] 
    [Range(5, 20)] [Tooltip("Determines the ricochet range towards the enemy.")] 
    public float rangeDetection;
    [Range(1, 10)] [Tooltip("Determines the amount of damage that drops when hitting an enemy.")] 
    public float damageAdaptableDrop;
}