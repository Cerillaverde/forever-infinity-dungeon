using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SOEnemy", menuName = "Scriptable Objects/SO Enemy")]
public class SOEnemy : ScriptableObject
{
    [Header("Atributtes of enemie")] [Space(2)]
    
    [Space(2)] public float experienceMin;
    [Space(2)] public float experienceMax;
    [Space(2)] public int goldMin;
    [Space(2)] public int goldMax;
    [Space(2)] public Color color;
    [Space(2)] public float maxHP;
    [Space(2)] public float moveSpeed;
    [Space(2)] public float damage;

    [Range(5, 20)] [Tooltip("Determines the enemy's detection range towards the player.")] [Space(2)]
    public float rangeDetection;

    [Range(1.2f, 10)] [Tooltip("Determines the enemy's attack range.")] [Space(2)]
    public float rangeAttack;
}