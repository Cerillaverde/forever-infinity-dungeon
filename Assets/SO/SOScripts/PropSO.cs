using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PropSO_", menuName = "PCG/PropSO")]
public class PropSO : ScriptableObject
{
    [Header("Prop data:")]
    
    public Sprite PropSprite;
    
    public Vector2Int PropSize = Vector2Int.one;
    
    [Space, Header("Placement type:")]
    public bool Corner = true;
    public bool NearWallUP = true;
    public bool NearWallDown = true;
    public bool NearWallRight = true;
    public bool NearWallLeft = true;
    public bool Inner = true;

    [Space, Header("Bool Conditions:")] 
    public bool LastRoom = true;
    public bool Effector = true;
    public bool ChestRoom = true;
    public bool Light = true;
    
    [Space, Header("Placement Quantity:")]
    
    [Min(1)]
    public int PlacementQuantityMin = 1;
    
    [Min(1)]
    public int PlacementQuantityMax = 1;

    [Space, Header("Group placement:")]
    public bool PlaceAsGroup = false;
    
    [Min(1)]
    public int GroupMinCount = 1;
     
    [Min(1)]
    public int GroupMaxCount = 1;
}
