using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;

    private int round;
    public int Round {  get => round; set => round = value;}
    private bool firstTime;
    public bool FirstTime { get => firstTime;  set => firstTime = value;}
    private bool deadButContinue;
    public bool DeadButContinue {  get => deadButContinue; }


    public AudioBGManager audioBGManager;
    public AudioSFXManager audioSFXManager;
    [SerializeField] private SOGeneralOptions SOPersistent;
    [SerializeField] private TMP_Text plantaInfo;

    [Space(10)] [Header("In Game Player 1 Canvas")] 
    [SerializeField] private GameObject playerCanvas;

    [SerializeField] private GameObject setFastItemCanvas;
    [SerializeField] private GameObject backpackPlayer1;
    [SerializeField] private Backpack startBackpack1;
    [SerializeField] private TMP_Text coinsCanvas;
    [SerializeField] private TMP_Text coinsCanvas2;
    [SerializeField] private TMP_Text playerLevel;
    [SerializeField] private Slider healthBar;
    [SerializeField] private Slider experienceBar;

    [Space(10)] [Header("In Game Player 2 Canvas")] 
    [SerializeField] private GameObject playerCanvas2;

    [SerializeField] private GameObject setFastItemCanvas2;
    [SerializeField] private GameObject backpackPlayer2;
    [SerializeField] private Backpack startBackpack2;
    [SerializeField] private TMP_Text playerLevel2;
    [SerializeField] private Slider healthBar2;
    [SerializeField] private Slider experienceBar2;

    [Space(10)] [Header("Menu Canvas")] 
    [SerializeField] private GameObject menuCanvas;
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject infoContainer;
    [SerializeField] private TMP_Text infoText;

    [Space(10)] [Header("Menu Shop")] 
    [SerializeField] private TMP_Text coinsCanvasShop;
    
    [Space(10)] [Header("Menu GameOver")] 
    [SerializeField] private GameObject gameOverCanvas;

    [Space(10)] [Header("Menu Options")] 
    [SerializeField] private GameObject optionsCanvas;
    [SerializeField] private GameObject helpCanvas;

    private bool gameIsPaused = false;

    public bool GetGameIsPaused()
    {
        return gameIsPaused;
    }

    [Space(10)] [Header("Inventory Canvas")] 
    [SerializeField] private GameObject player1InventoryCanvas;

    [SerializeField] private GameObject player2InventoryCanvas;
    [SerializeField] private DisplayBackpack displayBackpack1;
    [SerializeField] private DisplayBackpack displayBackpack2;
    [SerializeField] private DisplayShop displayShop;
    
    [SerializeField] private GameObject shopCanvas;

    private PlayerController player1Controller;
    private PlayerController player2Controller;

    [SerializeField] private GameObject cursorInventory;
    [SerializeField] private GameObject cursorShop;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this);
    }

    void Start()
    {
        firstTime = true;
        round = 0;
        menuCanvas.SetActive(false);
        pauseMenu.SetActive(false);
        gameOverCanvas.SetActive(false);
        optionsCanvas.SetActive(false);
        helpCanvas.SetActive(false);
        
        setFastItemCanvas.SetActive(false);
        setFastItemCanvas2.SetActive(false);
        shopCanvas.SetActive(false);
        playerCanvas.SetActive(false);
        playerCanvas2.SetActive(false);

        audioBGManager.SetVolume();
        //audioSFXManager.setVolume();
        LoadMenuScene();
    }

    //**************** PLAYER CONTROL ****************
    
    #region Player Control

    internal void SetPlayer(PlayerController playerController)
    {
        if (playerController.GetPlayer() == 1)
            player1Controller = playerController;
        if (playerController.GetPlayer() == 2)
            player2Controller = playerController;
    }

    public void DestroyPlayer(PlayerController playerController)
    {
        if (playerController.GetPlayer() == 1)
            Destroy(player1Controller);
        if (playerController.GetPlayer() == 2)
            Destroy(player2Controller);
    }

    public PlayerController GetPlayer(int player)
    {
        if (player == 1)
            return player1Controller;
        if (player == 2)
            return player2Controller;
        return null;
    }

    public void GiveExperience(float minExp, float maxExp)
    {
        if (player1Controller != null)
            player1Controller.ExperienceReward(Random.Range(minExp, maxExp));
        if (player2Controller != null)
            player2Controller.ExperienceReward(Random.Range(minExp, maxExp));
    }

    public void SetExprienceMaxBar(float experienceRequired, int player)
    {
        if (player == 1)
            experienceBar.maxValue = experienceRequired;
        if (player == 2)
            experienceBar2.maxValue = experienceRequired;
    }

    public void SetPlayerLevelCanvas(int level, int player)
    {
        if (player == 1)
            playerLevel.text = "Level " + level;

        if (player == 2)
            playerLevel2.text = "Level " + level;
    }

    public void SetExprienceBar(float experience, int player)
    {
        if (player == 1)
            experienceBar.value = experience;
        if (player == 2)
            experienceBar2.value = experience;
    }

    public void GiveGold(int minGold, int maxGold)
    {
        if (player1Controller != null)
            player1Controller.GoldReward(Random.Range(minGold, maxGold));
        if (player2Controller != null)
            player2Controller.GoldReward(Random.Range(minGold, maxGold));
    }

    public void SetGoldCanvas(int gold, int player)
    {
        if (player == 1)
            {
                coinsCanvas.text = " x" + gold + "";
                coinsCanvasShop.text = " x" + gold;
            }

        if (player == 2)
            { 
                coinsCanvas2.text = " x" + gold + "";
                coinsCanvasShop.text = " x"+gold;
            }
    }

    #endregion
    
    //**************** MANAGE SCENES ****************

    #region Manage Scene

    public void LoadMenuScene()
    {
        SceneManager.LoadScene("MenuScene");
        IntoMenuScene();
    }
    
    public void LoadShopScene()
    {
        SceneManager.LoadScene("ShopScene");
        IntoShopScene();
    }
    
    public void LoadDungeonScene()
    {
        SceneManager.LoadScene("DungeonScene");
        IntoDungeonScene();
    }
    
    public void LoadBossScene()
    {
        SceneManager.LoadScene("BoosFightScene");
        IntoBossFightScene();
    }

    public void LoadGameOverScene()
    {
        SceneManager.LoadScene("GameOverScene");
        IntoGameOverScene();
    }

    private void IntoMenuScene()
    {
        pauseMenu.SetActive(false);
        gameOverCanvas.SetActive(false);
        optionsCanvas.SetActive(false);
        setFastItemCanvas.SetActive(false);
        setFastItemCanvas2.SetActive(false);
        playerCanvas.SetActive(false);
        playerCanvas2.SetActive(false);
        shopCanvas.SetActive(false);
        plantaInfo.gameObject.SetActive(false);

        firstTime = true;
        round = 0;
        menuCanvas.SetActive(true);
        audioBGManager.MenuBackgroundAudioPlay();
    }

    private void IntoShopScene()
    {
        menuCanvas.SetActive(false);
        pauseMenu.SetActive(false);
        gameOverCanvas.SetActive(false);
        optionsCanvas.SetActive(false);
        setFastItemCanvas.SetActive(false);
        setFastItemCanvas2.SetActive(false);
        shopCanvas.SetActive(false);
        playerCanvas.SetActive(false);
        playerCanvas2.SetActive(false);
        
        ResumeGame();
        round++;
        plantaInfo.text = "Planta: " + (round * -1);
        plantaInfo.gameObject.SetActive(true);
        audioBGManager.ShopBackgroundAudioPlay();
        if (firstTime)
        {
            List<Backpack.ItemSlot> list1 = startBackpack1.GetItems();
            List<Backpack.ItemSlot> list2 = startBackpack2.GetItems();
            foreach (Backpack.ItemSlot slots in list1)
            {
                displayBackpack1.Backpack.ClearBackPack();
                displayBackpack1.Backpack.AddItem(slots.item);
                displayBackpack1.Backpack.GetItem(slots.item).amount = slots.amount;
            }
            foreach (Backpack.ItemSlot slots in list2)
            {
                displayBackpack2.Backpack.ClearBackPack();
                displayBackpack2.Backpack.AddItem(slots.item);
                displayBackpack2.Backpack.GetItem(slots.item).amount = slots.amount;
            }
            
            
        }
    }
    
    private void IntoDungeonScene()
    {
        menuCanvas.SetActive(false);
        pauseMenu.SetActive(false);
        gameOverCanvas.SetActive(false);
        optionsCanvas.SetActive(false);
        shopCanvas.SetActive(false);
        
        setFastItemCanvas.SetActive(true);
        setFastItemCanvas2.SetActive(true);
        playerCanvas.SetActive(true);
        playerCanvas2.SetActive(true);

        deadButContinue = false;
        firstTime = false;
        audioBGManager.DungeonBackgroundAudioPlay();
    }
    
    private void IntoBossFightScene()
    {
        menuCanvas.SetActive(false);
        pauseMenu.SetActive(false);
        gameOverCanvas.SetActive(false);
        optionsCanvas.SetActive(false);
        shopCanvas.SetActive(false);
        
        setFastItemCanvas.SetActive(true);
        setFastItemCanvas2.SetActive(true);
        playerCanvas.SetActive(true);
        playerCanvas2.SetActive(true);

        audioBGManager.BossBackgroundAudioPlay();
    }

    private void IntoGameOverScene()
    {
        menuCanvas.SetActive(false);
        pauseMenu.SetActive(false);
        optionsCanvas.SetActive(false);
        setFastItemCanvas.SetActive(false);
        setFastItemCanvas2.SetActive(false);
        shopCanvas.SetActive(false);
        playerCanvas.SetActive(false);
        playerCanvas2.SetActive(false);
        
        gameOverCanvas.SetActive(true);
        audioBGManager.GameOverBackgroundAudioPlay();
    }


    #endregion
    
    //**************** LEVEL CONTROL ****************

    #region Level Control
    
    public void SetMaxHealtBar(float health, int player)
    {
        if (player == 1)
            healthBar.maxValue = health;
        if (player == 2)
            healthBar2.maxValue = health;
    }

    public void SetHealtBar(float health, int player)
    {
        if (player == 1)
            healthBar.value = health;
        if (player == 2)
            healthBar2.value = health;
    }

    public void PauseGame(bool visible)
    {
        pauseMenu.SetActive(visible);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

    public void ResumeGame()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    public void ReturnToMenu()
    {
        ResumeGame();
        LoadMenuScene();
    }

    #endregion
    
    //**************** ABILITY COOLDOWN CANVAS ****************

    #region Ability Cooldown Canvas

    [Space(10)] [Header("Abilities Canvas")] [SerializeField]
    private float timeOfRefreshAbilityCanvas;

    [SerializeField] private Slider player1Ability1Slider;
    [SerializeField] private Slider player1Ability2Slider;
    [SerializeField] private Slider player2Ability1Slider;
    [SerializeField] private Slider player2Ability2Slider;
    [SerializeField] private Sprite ricochetSprite;
    [SerializeField] private Sprite boomerangSprite;
    [SerializeField] private Sprite explosionSprite;
    [SerializeField] private Sprite repelSprite;
    [SerializeField] private GameObject itemOptions1;
    [SerializeField] private GameObject itemOptions2;
    [SerializeField] private GameObject eventSystem;
    [SerializeField] private GameObject estoNoEsUnEventSystem;
    [SerializeField] private ShopController shop;


    public void ActiveOrDisableItemOptions(int player)
    {
        if (player == 1)
        {
            itemOptions1.SetActive(!itemOptions1.activeSelf);
        }

        if (player == 2)
        {
            itemOptions2.SetActive(!itemOptions2.activeSelf);
        }
    }


    public void SetAbilitiesCanvas(bool ricochetActive, bool explosionActive, int player)
    {
        if (ricochetActive)
        {
            if (player == 1)
                player1Ability1Slider.gameObject.GetComponent<Image>().sprite = ricochetSprite;
            if (player == 2)
                player2Ability1Slider.gameObject.GetComponent<Image>().sprite = ricochetSprite;
        }
        else
        {
            if (player == 1)
                player1Ability1Slider.gameObject.GetComponent<Image>().sprite = boomerangSprite;
            if (player == 2)
                player2Ability1Slider.gameObject.GetComponent<Image>().sprite = boomerangSprite;
        }


        if (explosionActive)
        {
            if (player == 1)
                player1Ability2Slider.gameObject.GetComponent<Image>().sprite = explosionSprite;
            if (player == 2)
                player2Ability2Slider.gameObject.GetComponent<Image>().sprite = explosionSprite;
        }
        else
        {
            if (player == 1)
                player1Ability2Slider.gameObject.GetComponent<Image>().sprite = repelSprite;
            if (player == 2)
                player2Ability2Slider.gameObject.GetComponent<Image>().sprite = repelSprite;
        }
    }

    public void Ability1CooldownCanvas(PlayerController playerController, float timeOfCooldown)
    {
        if (playerController.GetPlayer() == 1)
            StartCoroutine(Player1_Ability1Cooldown(timeOfCooldown));
        if (playerController.GetPlayer() == 2)
            StartCoroutine(Player2_Ability1Cooldown(timeOfCooldown));
    }

    public void Ability2CooldownCanvas(PlayerController playerController, float timeOfCooldown)
    {
        if (playerController.GetPlayer() == 1)
            StartCoroutine(Player1_Ability2Cooldown(timeOfCooldown));
        if (playerController.GetPlayer() == 2)
            StartCoroutine(Player2_Ability2Cooldown(timeOfCooldown));
    }

    IEnumerator Player1_Ability1Cooldown(float timeOfCooldown)
    {
        player1Ability1Slider.maxValue = timeOfCooldown;
        player1Ability1Slider.value = timeOfCooldown;

        while (player1Ability1Slider.value > 0)
        {
            player1Ability1Slider.value -= timeOfRefreshAbilityCanvas;
            yield return new WaitForSeconds(timeOfRefreshAbilityCanvas);
        }

        player1Ability1Slider.value = 0;
    }

    IEnumerator Player1_Ability2Cooldown(float timeOfCooldown)
    {
        player1Ability2Slider.maxValue = timeOfCooldown;
        player1Ability2Slider.value = timeOfCooldown;
        while (player1Ability2Slider.value > 0)
        {
            player1Ability2Slider.value -= timeOfRefreshAbilityCanvas;
            yield return new WaitForSeconds(timeOfRefreshAbilityCanvas);
        }

        player1Ability2Slider.value = 0;
    }

    IEnumerator Player2_Ability1Cooldown(float timeOfCooldown)
    {
        player2Ability1Slider.maxValue = timeOfCooldown;
        player2Ability1Slider.value = timeOfCooldown;

        while (player2Ability1Slider.value > 0)
        {
            player2Ability1Slider.value -= timeOfRefreshAbilityCanvas;
            yield return new WaitForSeconds(timeOfRefreshAbilityCanvas);
        }

        player2Ability1Slider.value = 0;
    }

    IEnumerator Player2_Ability2Cooldown(float timeOfCooldown)
    {
        player2Ability2Slider.maxValue = timeOfCooldown;
        player2Ability2Slider.value = timeOfCooldown;

        while (player2Ability2Slider.value > 0)
        {
            player2Ability2Slider.value -= timeOfRefreshAbilityCanvas;
            yield return new WaitForSeconds(timeOfRefreshAbilityCanvas);
        }

        player2Ability2Slider.value = 0;
    }

    #endregion
    
    //**************** INVENTORY CONTROL ****************

    #region Inventory Control

    public void ShowOrHideInventoryPlayer1()
    {
        backpackPlayer1.SetActive(!backpackPlayer1.activeSelf);
        player1InventoryCanvas.GetComponent<BackpackController>().updateUI();
        numPlayer = 1;
        estoNoEsUnEventSystem.SetActive(true);
        if (!backpackPlayer1.active)
        {
            player1InventoryCanvas.GetComponent<BackpackController>().DisableOptionsMenu();
        }
    }

    public void ShowOrHideInventoryPlayer2()
    {
        backpackPlayer2.SetActive(!backpackPlayer2.activeSelf);
        player2InventoryCanvas.GetComponent<BackpackController>().updateUI();
        numPlayer = 2;
        estoNoEsUnEventSystem.SetActive(false);
        eventSystem.SetActive(true);

        ActiveAndDidableCursorInventory();
        if (!backpackPlayer2.active)
        {
            player2InventoryCanvas.GetComponent<BackpackController>().DisableOptionsMenu(); 
            eventSystem.SetActive(false);
            estoNoEsUnEventSystem.SetActive(true);
        }
    }


    public BackpackController GetBackpackController(int num)
    {
        if (num == 1)
            return player1InventoryCanvas.GetComponent<BackpackController>();

        return player2InventoryCanvas.GetComponent<BackpackController>();
    }

    public void RefreshBackpackUI(int playernum)
    {
        if (playernum == 1)
            displayBackpack1.RefreshBackpack();
        if (playernum == 2)
            displayBackpack2.RefreshBackpack();
    }

    public void RefreshShop()
    {
        displayShop.RefreshShop();
    }

    private int numPlayer;

    public int GetNumPlayer()
    {
        return numPlayer;
    }

    public void SetNumPlayer(int numPlayer)
    {
        this.numPlayer = numPlayer;
    }

    public void ClickOnItem(ObjectClass objectClass)
    {
        if (numPlayer == 1)
        {
            player1InventoryCanvas.GetComponent<BackpackController>().ClickOnItem(objectClass);
        }

        if (numPlayer == 2)
        {
            player2InventoryCanvas.GetComponent<BackpackController>().ClickOnItem(objectClass);
        }
    }

    public void ClickOnItemShop(ObjectClass objectClass)
    {
        
        shop.BuyItem(objectClass);
    }

    public void ActiveAndDidableCursorInventory()
    {
        cursorInventory.SetActive(!cursorInventory.activeSelf);
    }
    public void ActiveAndDidableCursorShop()
    {
        cursorShop.SetActive(!cursorInventory.activeSelf);
    }

    #endregion
    
    //**************** OPTIONS CONTROL ****************

    #region Option Control

    public void ActiveCanvasOptions()
    {
        pauseMenu.SetActive(false);
        optionsCanvas.SetActive(true);
    }

    public void DisableCanvasOptions()
    {
        if (gameIsPaused)
            pauseMenu.SetActive(true);

        optionsCanvas.SetActive(false);
    }

    public void ContinueGame()
    {
        deadButContinue = true;
        round--;
        LoadShopScene();
    }

    public void ActiveCanvasHelp()
    {
        helpCanvas.SetActive(true);
    }
    
    public void DesctiveCanvasHelp()
    {
        helpCanvas.SetActive(false);
    }

    #endregion
    
    //**************** SHOP CONTROL ****************

    #region Shop Control

    public bool imShop;

    public void SetImShop(bool imShop)
    {
        this.imShop = imShop;
    }

    public void IntoShop(int player_num)
    {
        if (imShop)
        {

            Time.timeScale = 0f;
            
            shopCanvas.GetComponent<ShopController>().setNumPlayer(player_num);
            
            if (player_num == 1)
            {
                SetGoldCanvas(GameManager.Instance.GetPlayer(player_num).GetCoins(), player_num);
                eventSystem.SetActive(false);
                estoNoEsUnEventSystem.SetActive(true);
                shopCanvas.GetComponent<ShopController>().setNumPlayer(1);
                shopCanvas.SetActive(!shopCanvas.activeSelf);
                RefreshShop();
            }

            if (player_num == 2)
            {
                SetGoldCanvas(GameManager.Instance.GetPlayer(player_num).GetCoins(), player_num);
                estoNoEsUnEventSystem.SetActive(false);
                eventSystem.SetActive(true);
                ActiveAndDidableCursorShop();
                shopCanvas.GetComponent<ShopController>().setNumPlayer(2);
                shopCanvas.SetActive(!shopCanvas.activeSelf);
                RefreshShop();
            }

            if (!shopCanvas.active)
            {
                eventSystem.SetActive(false);
                estoNoEsUnEventSystem.SetActive(true);
                Time.timeScale = 1f;
            }
            
        }
    }

    public void UseItemFromBackpack(int player, int slot)
    {
        BackpackController bc;
        if (player == 1)
        {
            bc = GetBackpackController(player);
            bc.UseItem(slot);
        }

        if (player == 2)
        {
            bc = GetBackpackController(player);
            bc.UseItem(slot);
        }
    }

    #endregion

    //*************** OTHER *******************
    
    public void SetInfoMensaje(string message)
    {
        infoContainer.gameObject.SetActive(true);
        infoText.text = message;
        StartCoroutine(InfoTextInformea());
    }

    IEnumerator InfoTextInformea()
    {
        yield return new WaitForSeconds(.5f);
        
        infoContainer.gameObject.SetActive(false);
    }

    public void FromSaveDataPlayer1(SaveData.PlayerData data)
    {
        player1Controller.Load(data);
    }
    
    public void FromSaveDataPlayer2(SaveData.PlayerData data)
    {
        player2Controller.Load(data);
    }

    public void SaveCurrentsPlayersInGM()
    {
        player1Controller.SaveCurrentPlayer();
        player2Controller.SaveCurrentPlayer();
    }

    public void LoadArmorAndWeaponOnBackpack()
    {
        if (player1Controller.Armor.getName() != "Armor")
        {
            displayBackpack1.Backpack.AddItem(player1Controller.Armor);  
        }

        if (player1Controller.Weapon.getName() != "Weapon")
        {
            displayBackpack1.Backpack.AddItem(player1Controller.Weapon);
        }

        if (player2Controller.Weapon.getName() != "Weapon")
        {
            displayBackpack2.Backpack.AddItem(player2Controller.Weapon);
        }
        
        if (player2Controller.Armor.getName() != "Armor")
        {
           
            displayBackpack2.Backpack.AddItem(player2Controller.Armor);
        }
        
        
        
    }


}